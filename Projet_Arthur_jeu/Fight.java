
/**
 * Interface Fight.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public interface Fight
{
    /**
     * Constantes de puissance
     */
    public static final int POWER = 10;
    
    /**
     * Method enchant to cast a fate on a hero 
     * 
     * @param   hero    the character to be enchanted
     * @return  the hero's life points lost after the enchant  
     */
    public int enchant(Character hero);
    
    /**
     * Method attack to attack a hero 
     * 
     * @param   hero    the character to be attacked 
     */
    public void attack(Character hero);
    
    /**
     * Method defend to defend a character from an attack
     * 
     * @return  the character's life points lost after the defence  
     */
    public int defend();
    
    /**
     * Method rob to rob a hero's items 
     * 
     * @param   hero    the character to be attacked 
     */
    public void rob(Character hero);
}