/**
 * Cette classe est une partie du jeu "Arthur, la conquête du Graal"   
 * 
 * Cette classe détient toutes les commandes que le jeu connait 
 * pour les actions actives du jeu
 * (Attaquer, défendre, boire un potion et fuir le combat).
 *
 * @author  Husref Baltic
 * @version 1.0
 */

public class CommandFight
{
    // a constant array that holds all valid command words
    private static final String[] VALID_COMMANDS = {
        "att", "def", "drink", "esc"
    };

    /**
     * Constructeur, permet d'initialiser les commandes.
     */
    public CommandFight()
    {
        // nothing to do at the moment...
    }

    /**
     * Vérifie si la commande passé en paramaètre est valide ou non. 
     * @param aString La commande à valider.
     * @return true si la commande existe, false sinon.
     */
    public boolean isCommandFight(String aString)
    {
        for (int i = 0; i < VALID_COMMANDS.length; i++) {
            if (VALID_COMMANDS[i].equals(aString))
                return true;
        }
        // if we get here, the string was not found in the commands
        return false;
    }
}
