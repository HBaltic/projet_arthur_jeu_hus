import java.io.Serializable;
import java.util.HashMap;
/**
 * Cette classe référence tout les attributs à sauvegarder
 *
 * @author Husref
 * @version 1.0
 */
public class Save implements Serializable
{
    private Hero arthur;
    private RoomsField roomsField;
    private Room currentRoom;
    private Room extGrotte;
    private Room intGrotte;
    private Room cmpTruand;
    private Room bossCapalus;
    private Room cmpGobe;
    private Room couloiSerpent;
    private Room bossGlatissante;
    private Room salleLancelot;
    private Room salleEnigme;
    private Room bossGobe;
    private Room salleMarchand;
    private Room tresorGobe;
    private Room corridorD;
    private Room bossDragon;
    private Room salleTele;
    private Room grall;
    private HashMap<Integer, Object> obj;

    /**
     * Constructeur vide
     */
    public Save() {
    }
    
    /**
     * Setter de l'attribut extGrotte
     * @param r     Room 
     */
    public void setExtGrotte(Room r) {
        extGrotte = r;
    }
    
    /**
     * Getter de l'attribut extGrotte
     * @return extGrotte
     */
    public Room getExtGrotte() {
        return extGrotte;
    }
    
     /**
     * Setter de l'attribut intGrotte
     *@param r     Room 
     */
    public void setIntGrotte(Room r) {
        intGrotte = r;
    }
    
    /**
     * Getter de l'attribut intGrotte
     * @return intGrotte
     */
    public Room getIntGrotte() {
        return intGrotte;
    }
    
     /**
     * Setter de l'attribut cmpTruand
     * @param r     Room
     */
    public void setCmpTruand(Room r) {
        cmpTruand = r;
    }
    
         /**
     * Getter de l'attribut cmpTruand
     * @return cmpTruand
     */
    public Room getCmpTruand() {
        return cmpTruand;
    }
    
     /**
     * Setter de l'attribut bossCapalus
     *@param r     Room 
     */
    public void setBossCapalus(Room r) {
        bossCapalus = r;
    }
    
         /**
     * Getter de l'attribut bosCapalus
     * @return bossCapalus
     */
    public Room getBossCapalus() {
        return bossCapalus;
    }
    
         /**
     * Setter de l'attribut cmpGobe
      *@param r     Room 
     */
    public void setCmpGobe(Room r) {
        cmpGobe = r;
    }
    
         /**
     * Getter de l'attribut cmpGobe
     * @return cmpGobe
     */
    public Room getCmpGobe() {
        return cmpGobe;
    }
    
         /**
     * Setter de l'attribut couloiSerpent
      *@param r     Room 
     */
    public void setCouloiSerpent(Room r) {
        couloiSerpent = r;
    }
    
         /**
     * Getter de l'attribut couloiSerpent
     * @return couloiSerpent
     */
    public Room getCouloiSerpent() {
        return couloiSerpent;
    }
    
             /**
     * Setter de l'attribut bossGlatissante
      *@param r     Room 
     */
    public void setBossGlatissante(Room r) {
        bossGlatissante = r;
    }
    
         /**
     * Getter de l'attribut bossGlatissante
     * @return bossGlatissante
     */
    public Room getBossGlatissante() {
        return bossGlatissante;
    }
   
    /**
     * Setter de l'attribut salleLancelot
      *@param r     Room 
     */
    public void setSalleLancelot(Room r) {
        salleLancelot = r;
    }
    
         /**
     * Getter de l'attribut salleLancelot
     * @return salleLancelot
     */
    public Room getSalleLancelot() {
        return salleLancelot;
    }
     
    /**
     * Setter de l'attribut salleEnigme
      *@param r     Room 
     */
    public void setSalleEnigme(Room r) {
        salleEnigme = r;
    }
    
         /**
     * Getter de l'attribut salleEnigme
     * @return salleEnigme
     */
    public Room getSalleEnigme() {
        return salleEnigme;
    }
    
        /**
     * Setter de l'attribut bossGobe
      *@param r     Room 
     */
    public void setBossGobe(Room r) {
        bossGobe = r;
    }
    
         /**
     * Getter de l'attribut bossGobe
     * @return bossGobe
     */
    public Room getBossGobe() {
        return bossGobe;
    }
    
            /**
     * Setter de l'attribut salleMarchand
      *@param r     Room 
     */
    public void setSalleMarchand(Room r) {
        salleMarchand = r;
    }
    
         /**
     * Getter de l'attribut salleMarchand
     * @return salleMarchand
     */
    public Room getSalleMarchand() {
        return salleMarchand;
    }
    
    /**
     * Setter de l'attribut tresorGobe
      *@param r     Room 
     */
    public void setTresorGobe(Room r) {
        tresorGobe = r;
    }
    
         /**
     * Getter de l'attribut tresorGobe
     * @return tresorGobe
     */
    public Room getTresorGobe() {
        return tresorGobe;
    }
    
        /**
     * Setter de l'attribut corridorD
      *@param r     Room 
     */
    public void setCorridorD(Room r) {
        corridorD = r;
    }
    
         /**
     * Getter de l'attribut corridorD
     * @return corridorD
     */
    public Room getCorridorD() {
        return corridorD;
    }
    
            /**
     * Setter de l'attribut bossDragon
      *@param r     Room 
     */
    public void setBossDragon(Room r) {
        bossDragon = r;
    }
    
         /**
     * Getter de l'attribut bossDragon
     * @return bossDragon
     */
    public Room getBossDragon() {
        return bossDragon;
    }
    
     /**
     * Setter de l'attribut salleTele
      *@param r     Room 
     */
    public void setSalleTele(Room r) {
        salleTele = r;
    }
    
         /**
     * Getter de l'attribut salleTele
     * @return salleTele
     */
    public Room getSalleTele() {
        return salleTele;
    }
    
         /**
     * Setter de l'attribut grall
      *@param r     Room 
     */
    public void setGrall(Room r) {
        grall = r;
    }
    
         /**
     * Getter de l'attribut grall
     * @return grall
     */
    public Room getGrall() {
        return grall;
    }

    /**
     * Getter de l'attribut obj
     * @return obj
     */
    public HashMap<Integer, Object> getObj() {
        return obj;
    }
    
    /**
     * Setter de l'attribut obj
     * @param o     HashMap<Integer, Object> 
     */
    public void setObj(HashMap<Integer, Object> o) {
        obj = o;
    }
    
    /**
     * Getter de l'attribut currentRoom
     * @return currentRoom
     */
    public Room getCurrentRoom() {
        return currentRoom;
    }
    
    /**
     * Setter de l'attribut currentRoom
      *@param r     Room 
     */
    public void setCurrentRoom(Room r) {
        currentRoom = r;
    }
    
    /**
     * Getter de l'attribut arthur
     * @return arthur
     */
    
    public Hero getArthur() {
        return arthur;
    }
    
    /**
     * Getter de l'attribut roomsField
     * @return roomsField
     */
    
    public RoomsField getRoomsField() {
        return roomsField;
    }
    
    /**
     * Setter de l'attribut arthur
     * @param h hero
     */
    
    public void setArthur(Hero h) {
        arthur = h;
    }
    
    /**
     * Setter de l'attribut cRoom
     * @param rf Room
     */
    
    public void setRoomsField(RoomsField rf) {
        roomsField = rf;
    }
}
