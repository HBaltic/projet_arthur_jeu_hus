import java.io.Serializable;
/**
 * t class Item
 * 
 * @author (tetsatls)
 * @version (v1)
 */
public class Item implements Serializable
{
    // instance variables
    private String name;
    
    /**
     * Constructeur de la classe qui prend un paramètre
     * @param name le nom de l'item
     */
    public Item(String name) {
        this.name = name;
    }
    
    /**
     * Method getName
     * 
     * @return  the name of the item
     */
    public String getName() {
        return this.name;
    }
      
}