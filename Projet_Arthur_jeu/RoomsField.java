import java.util.*;
import java.io.Serializable;
/**
 * Represent a rectangular grid of field positions.
 * Each position is able to store a single animal.
 * 
 * @author habsaoui2u
 * @version 2015.03.21
 */
public class RoomsField implements Serializable
{    
    // The width of the field in pixel
    private int width;
    // The height of the field in pixel
    private int height;
    // Storage for the rooms.
    private Room[][] field;

    /**
     * Represent a field of the given dimensions.
     * @param   height  The height of the field.
     * @param   width   The width of the field.
     */
    public RoomsField(int width, int height)
    {
        this.width = width;
        this.height = height;        
        this.field = new Room[width][height];
        this.clear();
    }
    
    /**
     * Empty the field.
     */
    public void clear()
    {
        for (int y = 0; y < height ; y++) {
            for (int x = 0; x < width; x++) {
                this.field[x][y] = null;
            }
        }
    }
    
    /**
     * Clear the given location.
     * @param x coordonnée des abscisse.
     * @param y coordonnée des ordonnées
     */
    public void clear(int x, int y)
    {
        this.field[x][y] = null;
    }
    
    /**
     * Return the room at the given location, if any.
     * 
     * @param   x   Row coordinate of the location.
     * @param   y   Column coordinate of the location.
     * @return  The room at the given location, or null if there is none.
     */
    public Room getRoom(int x, int y)
    {
        return this.field[x][y];
    }
    
        /**
     * Place a room at the given location.
     * 
     * @param room  The room to be placed.
     * @param   x   Row coordinate of the location.
     * @param   y   Column coordinate of the location.
     */
    public void setRoom(Room room, int x, int y)
    {
        this.field[x][y] = room;
    }
        
    /**
     * Return all the rooms field.
     * 
     * @return  The rooms field.
     */
    public Room[][] getField()
    {
        return this.field;
    }
    
    /**
     * Set all the rooms field with a new one.
     * 
     * @param   newField    The new field to set up.
     */
    public void setField(Room[][] newField)
    {
        this.field = newField;
    }
    
    /**
     * Return the height of the field.
     * @return The height of the field.
     */
    public int getHeight()
    {
        return this.height;
    }
    
    /**
     * Return the width of the field.
     * @return The width of the field.
     */
    public int getWidth()
    {
        return this.width;
    }
    
    /**
     * Méthode permettant de transformer une
     * donnée roomsField en objet roomsField
     * @param data  donnée à transformer
     */
    
    public void transformIn(RoomsField data) {
        this.height = data.getHeight();
        this.width = data.getWidth();
        for (int i = 0; i < this.height; i++) {
            for (int j = 0 ; i < this.width ; i++) {
                this.field = data.getField();
            }
        }
    }
}
