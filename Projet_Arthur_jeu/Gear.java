
/**
 * Description de la classe Gear qui hérite de la classe Item
 * 
 * @author (tetsatls) 
 * @version (v1)
 */
public class Gear extends Item
{
    //attributs de la potion
    private String name;
    private int attPlus;
    private int defPlus;
    private int chancePlus;
    
    /**
     * constructeur de classe qui prend 4 paramètres
     * @param name name le nom du gear
     * @param attPlus attPlus
     * @param defPlus defPlus
     * @param chancePlus chancePlus
     */
    public Gear(String name, int attPlus, int defPlus, int chancePlus) {
        super(name);
        this.attPlus = attPlus;
        this.defPlus = defPlus;
        this.chancePlus = chancePlus;
    }
    
    /**
     * Getter qui retourne attPlus
     * @return l'attribut attPlus
     */
    public int getAttPlus() {
        return this.attPlus;
    }
    
    /**
     * Getter qui retourne def
     * @return l'attribut defPlus
    */
    public int getDefPlus() {
        return this.defPlus;
    }
    
    /**
     * Getter qui retourne chancePlus
     * @return l'attribut chancePlus
     */
    public int getChancePlus() {
        return this.chancePlus;
    }
   
    
}