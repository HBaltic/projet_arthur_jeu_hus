import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


/**
 * Classe-test HeroTest.
 */
public class MonsterTest
{
    /**
     * Constructeur de la classe-test HeroTest
     */
    public MonsterTest()
    {
    }
   
    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
      
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }
    
        /**
     * Test getteur, setteur
     */
    @Test
    public void testGetNom(){
        Monster monster = new Monster("Teemo","Houlala",300,100,150);
        assertEquals("Est-ce-que le nom est correct","Teemo",monster.getName());
    }
    
     @Test
    public void testGetDescription(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        assertEquals("Est-ce-que le nom est correct","Houlala",monster.getDescription());
    }
    
    @Test
    public void testGetLife(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        assertEquals("Est-ce-que les pv sont correct",300,monster.getLife());
    }
    
    @Test
    public void testGetDefence(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        assertEquals("Est-ce-que la défense est correct",100,monster.getDefence());
    }
    
     @Test
    public void testGetDamage(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        assertEquals("Est-ce-que l'attaque est correct",150,monster.getDamage());
    }
    
     @Test
    public void testGetSort(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        assertEquals("Est-ce-que le sort est correct",2,monster.getSort());
    }
    
     @Test
    public void testSetNom(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        monster.setName("Husref");
        assertEquals("Est-ce-que le nom est correct","Husref",monster.getName());
    }
    
     @Test
    public void testSetDescription(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        monster.setDescription("BadaBoum!");
        assertEquals("Est-ce-que le nom est correct","BadaBoum!",monster.getDescription());
    }
    
    @Test
    public void testSetLife(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        monster.setLife(-4);
        assertEquals("Est-ce-que les pv sont correct",0,monster.getLife());
        monster.setLife(200);
        assertEquals("Est-ce-que les pv sont correct",200,monster.getLife());
    }
    
    @Test
    public void testSetDefence(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        monster.setDefence(20);
        assertEquals("Est-ce-que la défense est correct",20,monster.getDefence());
    }
    
     @Test
    public void testSetDamage(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        monster.setDamage(50);
        assertEquals("Est-ce-que l'attaque est correct",50,monster.getDamage());
    }
    
    @Test
    public void testfate(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        Hero hero = new Hero("Arthur","Houlala",300,125,175,1.2);
        monster.fate(hero);
        
        assertEquals(125,hero.getLife());
        assertEquals(280,monster.getLife());
        
    }
    
    @Test
    public void testattack(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        Hero hero = new Hero("Arthur","Houlala",300,125,175,1.2);
        monster.attack(hero);
        
        assertEquals(275,hero.getLife());
        assertEquals(300,monster.getLife());
        
    }
    
    @Test
    public void testgetSort(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        assertEquals(2,monster.getSort());
    }
    
    @Test
    public void testDefend(){
        Monster monster = new Monster("Arthur","Houlala",300,100,150);
        Hero arthur = new Hero("arthur", "description", 300, 100, 150, 1.1);
        
        monster.defend(arthur);
        assertEquals(105,monster.getDefence());
        
        Monster monster1 = new Monster("Arthur","Houlala",300,40,150);
        monster1.defend(arthur);
        assertEquals(45,monster1.getDefence());
        
    }
    
}