

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class GearTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class GearTest
{
    /**
     * Default constructor for test class GearTest
     */
    public GearTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    /**
     * Méthode permettant de tester le getter de l'attribut attPlus 
     */
    
    @Test
    public void attPlusTest()
    {
        Gear gear1 = new Gear("test", 120, 110, 30);
        assertEquals(120, gear1.getAttPlus());
    }

     /**
     * Méthode permettant de tester le getter de l'attribut defPlus 
     */
    
    @Test
    public void defPlusTest()
    {
        Gear gear1 = new Gear("test", 120, 110, 30);
        assertEquals(110, gear1.getDefPlus());
    }

     /**
     * Méthode permettant de tester le getter de l'attribut chancePlus 
     */
    
    @Test
    public void chancePlusTest()
    {
        Gear gear1 = new Gear("test", 12, 12, 30);
        assertEquals(30, gear1.getChancePlus());
    }
}



