import java.util.*;
import java.io.Serializable;
/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "Project_Arthur_the_Game" application. 
 * "Arthur_the_Game" is a very simple, 
 * text and/or graphical based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  The exits are labelled north, 
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 * 
 * @author  habsaoui2u
 * @version 2015.03.11
 */
public class Room implements Serializable
{
    // Room's description as a line of text. 
    private String description;
    
    // Room at the north of that room or null.
    private Room northExit;
    // Room at the north of that room or null.
    private Room southExit;
    // Room at the north of that room or null.
    private Room eastExit;
    // Room at the north of that room or null.
    private Room westExit;    
    // true if the room has been visited
    private boolean visited; 
    
    /**
     * Create a room described "description". Initially, it has no exits.
     * 
     * @param description The room's description.
     */
    public Room(String description) 
    {
        this.description = description;
        this.visited = false;
    }

    /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     */
    public void setExits(Room north, Room east, Room south, Room west) 
    {
        if (north != null)
            this.northExit = north;
        if (east != null)
            this.eastExit = east;
        if (south != null)
            this.southExit = south;
        if (west != null)
            this.westExit = west;
    }

    /**
     * @return The description of the room.
     */
    public String getDescription()
    {
        return this.description;
    }
    
    /**
     * @return The next room at the North exit.
     */
    public Room getNorthExit()
    {
        return this.northExit;
    }
    
    
    /**
     * @return The next room at the North exit.
     */
    public Room getSouthExit()
    {
        return this.southExit;
    }
    
    
    /**
     * @return The next room at the North exit.
     */
    public Room getEastExit()
    {
        return this.eastExit;
    }
    
    
    /**
     * @return The next room at the North exit.
     */
    public Room getWestExit()
    {
        return this.westExit;
    }
    
    /**
     * @return  true if the room has been visited.
     */
    public boolean getVisited()
    {
        return this.visited;
    }
    
    /**
     * Set the room as already visited.
     */
    public void setVisited()
    {
        this.visited = true;
    }
    
    /**
     * Méthode permettant de tranformer des données room en objet room
     * @param data  donnée à transformer
     */
    
    public void transformIn(Room data) {
        this.description = data.getDescription();
        this.visited = data.getVisited();
        this.northExit = data.getNorthExit();
        this.southExit = data.getSouthExit();
        this.eastExit = data.getEastExit();
        this.westExit = data.getWestExit();
    }
}
