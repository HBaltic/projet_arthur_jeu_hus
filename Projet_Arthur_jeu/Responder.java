import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
/**
 * Classe qui gère les énigmes posées au héro
 * 
 * @author tetsatls
 * @version V1
 */
public class Responder
{
    ArrayList<String> defaultResponses;
    private Random randomGenerator;
    HashMap<String, String> responseMap;
    HashMap<String, String> questionMap;
    
    /**
     * Constructeur par défaut qui initialise les
     * différentes collections de la classe
     */
    Responder()
    {
   
        randomGenerator = new Random();   
        defaultResponses = new ArrayList<String>();
        responseMap = new HashMap <String, String>();
        questionMap = new HashMap<String, String>();
    }
    
    /**
    * Méthode qui insère des données dans la 
    * collection de réponse responseMap
    */
    public void fillResponseMap() {
        responseMap.put("La faim", "La faim");
        responseMap.put("Hier Hervé a acheté des cuillières", 
            "Hier Hervé a acheté des cuillières");
        responseMap.put("Mon père", "Mon père");
    }
    
    /**
    * Méthode qui insère des données dans 
    * l'arraylist de réponses par défaut
    */
    public void fillDefaultResponses() {
        defaultResponses.add("Désolé mais ce n'est pas la bonne réponse...");
        defaultResponses.add("Essayer de nouveau Sir, vous allez y arriver!");
        defaultResponses.add("Réfléchissez et lisez bien la question, cela vous aidera grandement.");
        defaultResponses.add("Vous avez trouvé... La mauvaise réponse! Essayer de nouveau.");
        
    }
    
    /**
    * Méthode qui insère des données dans la collection de questions
    */
    public void fillQuestionMap() {
        questionMap.put("La faim",
            "Encore fléau à notre époque,\n" 
            + "Par le loup souvent on l'évoque, \n" 
            + "On peut hélas en souffrir, \n" 
            + "Parfois même au point d'en mourir.\n" 
            + "Qui suis-je ?");
        questionMap.put("Hier Hervé a acheté des cuillières", 
            "Des chevaliers cathares souhaitent organiser une réunion scrète. \n" 
            + "Pour éviter l'instrusion de traîtres, \n" 
            + "ils créent le mot de passe suivant : \n" 
            + "     IRRVAHTDQIR \n" + "Quel moyen ont-ils trouvé pour le mémoriser?");
        questionMap.put("Mon père", "Un homme regarde un portrait et affirme : <<Je n'ai point de frère ni de soeur, \n" 
            + "mais pourtant le fils de l'homme sur ce portrait est le fils de mon père.>> \n" 
            + "De qui cet homme regarde-t-il le portrait ?");        
    }
    
    /**
    * Méthode qui génère aléatoirement une réponses par défaut
    * @return   responses
    */
    public String pickDefaultResponse() {        
        return defaultResponses.get((randomGenerator.nextInt(defaultResponses.size())));
    }
}
