import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


/**
 * Classe-test HeroTest.
 */
public class HeroTest
{
    /**
     * Constructeur de la classe-test HeroTest
     */
    public HeroTest()
    {
    }
   
    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
      
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }
    
        /**
     * Test getteur, setteur
     */
    @Test
    public void testGetNom(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        assertEquals("Est-ce-que le nom est correct","Arthur",hero.getName());
    }
    
     @Test
    public void testGetDescription(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        assertEquals("Est-ce-que le nom est correct","Houlala",hero.getDescription());
    }
    
    @Test
    public void testGetLife(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        assertEquals("Est-ce-que les pv sont correct",300,hero.getLife());
    }
    
    @Test
    public void testGetDefence(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        assertEquals("Est-ce-que la défense est correct",100,hero.getDefence());
    }
    
     @Test
    public void testGetDamage(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        assertEquals("Est-ce-que l'attaque est correct",150,hero.getDamage());
    }
    
     @Test
    public void testGetChance(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        assertEquals(1.2,hero.getChance(),0);
    }
    
     @Test
    public void testGetBag(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        HashMap <Integer,Object> bag= new HashMap<Integer,Object>();
        assertEquals("",bag,hero.getBag());
    }
    
     @Test
    public void testSetNom(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        hero.setName("Husref");
        assertEquals("Est-ce-que le nom est correct","Husref",hero.getName());
    }
    
     @Test
    public void testSetDescription(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        hero.setDescription("BadaBoum!");
        assertEquals("Est-ce-que le nom est correct","BadaBoum!",hero.getDescription());
    }
    
    @Test
    public void testSetLife(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        hero.setLife(-4);
        assertEquals("Est-ce-que les pv sont correct",0,hero.getLife());
        hero.setLife(200);
        assertEquals("Est-ce-que les pv sont correct",200,hero.getLife());
    }
    
    @Test
    public void testSetDefence(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        hero.setDefence(20);
        assertEquals("Est-ce-que la défense est correct",20,hero.getDefence());
    }
    
     @Test
    public void testSetDamage(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        hero.setDamage(50);
        assertEquals("Est-ce-que l'attaque est correct",50,hero.getDamage());
    }
    
     @Test
    public void testSetChance(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        hero.setChance(1.5);
        assertEquals(1.5,hero.getChance(),0);
    }
    
    @Test
    public void testaddBag(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        Potion potion= new Potion("tata", 5);
        hero.addBag(potion);
        Integer mapKey=4;
        assertEquals(potion,hero.getBag().get(mapKey));
    }
    
    @Test
    public void testtransformIn(){
        Hero hero = new Hero("Arthur","Houlala",300,100,150,1.2);
        Hero data = new Hero("Arthur2","Houlala2",3020,1200,1250,12.2);
        hero.transformIn(data);
        
      
        assertEquals("Est-ce-que le nom est correct",hero.getName(),data.getName());
        assertEquals("Est-ce-que la description est correct",hero.getDescription(),data.getDescription());
        assertEquals("Est-ce-que les pv sont correct",hero.getLife(),data.getLife());
        assertEquals("Est-ce-que la défense est correct",hero.getDefence(),data.getDefence());
        assertEquals("Est-ce-que l'attaque est correct",hero.getDamage(),data.getDamage());
        assertEquals(hero.getChance(), data.getChance(),0);
        for(int i = 4; i<10; i++){
        assertEquals(data.getBag().get(i),hero.getBag().get(i));
        
    }
}
}


