

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

/**
 * Classe-test SaveTest.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 *
 * Les classes-test sont documentées ici :
 * http://junit.sourceforge.net/javadoc/junit/framework/TestCase.html
 * et sont basées sur le document Š 2002 Robert A. Ballance intitulé
 * "JUnit: Unit Testing Framework".
 *
 * Les objets Test (et TestSuite) sont associés aux classes à tester
 * par la simple relation yyyTest (e.g. qu'un Test de la classe Name.java
 * se nommera NameTest.java); les deux se retrouvent dans le męme paquetage.
 * Les "engagements" (anglais : "fixture") forment un ensemble de conditions
 * qui sont vraies pour chaque méthode Test à exécuter.  Il peut y avoir
 * plus d'une méthode Test dans une classe Test; leur ensemble forme un
 * objet TestSuite.
 * BlueJ découvrira automatiquement (par introspection) les méthodes
 * Test de votre classe Test et générera la TestSuite conséquente.
 * Chaque appel d'une méthode Test sera précédé d'un appel de setUp(),
 * qui réalise les engagements, et suivi d'un appel à tearDown(), qui les
 * détruit.
 */
public class SaveTest
{
    // Définissez ici les variables d'instance nécessaires à vos engagements;
    // Vous pouvez également les saisir automatiquement du présentoir
    // à l'aide du menu contextuel "Présentoir --> Engagements".
    // Notez cependant que ce dernier ne peut saisir les objets primitifs
    // du présentoir (les objets sans constructeur, comme int, float, etc.).
    /**
     * Attributs pour classe test par defaut
     */
    protected double fValeur1;
    /**
     * Attributs pour classe test par defaut
     */
    protected double fValeur2;

    /**
     * Constructeur de la classe-test SaveTest
     */
    public SaveTest()
    {
    }

    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
        // Initialisez ici vos engagements
        fValeur1 = 2.0;
        fValeur2 = 3.0;
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }
    
    /**
     * Méthode test pour getter arthur
     */

    @Test
    public void testGetterArthur()
    {
        Save save1 = new Save();
        Hero arthur = new Hero("arthur", "description", 150, 50, 50, 1.1);
        save1.setArthur(arthur);
        assertEquals(arthur, save1.getArthur());
    }
    
    /**
     * Méthode test pour getter room
     */
    
    @Test
    public void testGetterRoom()
    {
        Save save1 = new Save();
        Room roomTest = new Room("descritpion");
        save1.setCurrentRoom(roomTest);
        assertEquals(roomTest, save1.getCurrentRoom());
    }
    
    /**
     * Méthode test pour getter obj
     */
    
    @Test
    public void testGetterObj()
    {
        Save save1 = new Save();
        HashMap<Integer, Object> obj = new HashMap<Integer, Object>();
        save1.setObj(obj);
        assertEquals(obj, save1.getObj());
    }
    
    /**
     * Méthode test pour getter roomsField
     */
    
    @Test
    public void testGetterRoomsField()
    {
        Save save1 = new Save();
        RoomsField  rf = new RoomsField(10, 10);
        save1.setRoomsField(rf);
        assertEquals(rf, save1.getRoomsField());
    }
    
    /**
     * Méthode test pour setter roomsField
     */
    
    @Test
    public void testSetterRoomsField()
    {
        Save save1 = new Save();
        RoomsField  rf = null;
        save1.setRoomsField(rf);
        assertEquals(null, save1.getRoomsField());
        
        RoomsField  rf2 = new RoomsField(15, 15);
        save1.setRoomsField(rf2);
        assertEquals(rf2, save1.getRoomsField());
    }
    
    /**
     * Méthode test pour setter obj
     */
    
    @Test
    public void testSetterObj()
    {
        Save save1 = new Save();
        HashMap<Integer, Object> obj = null;
        save1.setObj(obj);
        assertEquals(null, save1.getObj());
        
        HashMap<Integer, Object> obj2 = new HashMap<Integer, Object>();
        save1.setObj(obj2);
        assertEquals(obj2, save1.getObj());
    }
    
    /**
     * Méthode test pour setter room
     */
    
    @Test
    public void testSetterRoom()
    {
        Save save1 = new Save();
        Room roomTest = null;
        save1.setCurrentRoom(roomTest);
        assertEquals(null, save1.getCurrentRoom());
        
        Room roomTest2 = new Room("descritpion");
        save1.setCurrentRoom(roomTest2);
        assertEquals(roomTest2, save1.getCurrentRoom());
        
    }
    
    /**
     * Méthode test pour setter arthur
     */
    
    @Test
    public void testSetterArthur()
    {
        Save save1 = new Save();
        Hero arthur = null;
        save1.setArthur(arthur);
        assertEquals(null, save1.getArthur());
        
        Hero arthur2 = new Hero("arthur", "description", 150, 50, 50, 1.1);
        save1.setArthur(arthur2);
        assertEquals(arthur2, save1.getArthur());
    }
    
}

