

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PotionTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PotionTest
{
    /**
     * Default constructor for test class PotionTest
     */
    public PotionTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    /**
     * Méthode permettant de tester le getter de l'attribut plus 
     */
    
    @Test
    public void plusTest()
    {
        Potion potion1 = new Potion("test", 30);
        assertEquals(30, potion1.getPlus());
    }
}

