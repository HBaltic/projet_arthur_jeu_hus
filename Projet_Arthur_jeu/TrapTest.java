

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TrapTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class TrapTest
{
    /**
     * Default constructor for test class TrapTest
     */
    public TrapTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    /**
     * Méthode test pour le getter de l'attribut minus
     */
    @Test
    public void minusTest()
    {
        Trap trap1 = new Trap("test", 20);
        assertEquals(20, trap1.getMinus());
    }
}

