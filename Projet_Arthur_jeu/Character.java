import java.io.Serializable;
/**
 * Abstract class Character 
 * @author Matthéo
 * @version 1.0
 */
public abstract class Character implements Serializable
{
    // inheritance variables
    private String name;
    private String description;
    private int life;
    private int defence;
    private int damage;
    

    /**
     * Constructor of class
     * @param n the name of the character
     * @param desc the description of the character
     * @param life the life level of the character
     * @param def the defence level of the character
     * @param d the damage level of the character
     */
    public Character(String n, String desc, int life, int def, int d)
    {
        this.name = n;
        this.description = desc;
        this.life = life;
        this.defence = def;
        this.damage = d;
        
        
    }

    /**
     * Get the character's name
     * 
     * @return name the name of the character
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * Get the character's description
     * 
     * @return description the description of the character
     */
    public String getDescription()
    {
        return this.description;
    }
    
    /**
     * Get the character's description
     * 
     * @return life the life level of the character
     */
    public int getLife()
    {
        return this.life;
    }
    
    /**
     * Get the character's description
     * 
     * @return defence the defence level of the character
     */
    public int getDefence()
    {
        return this.defence;
    }
    
    /**
     * Get the character's description
     * 
     * @return life true if character is still alive
     */
    public boolean isAlive()
    {
        return this.life > 0;
    }
    
     /**
     * Get the character's damage
     * 
     * @return damage the damage of the character
     */
    public int getDamage()
    {
        return this.damage;
    }
    
     /**
     * Set the character's name
     * @param n new name
     */
    public void setName(String n)
    {
        this.name = n;
    }
    
      /**
     * Set the character's defence
     * @param d new defence level
     */
    public void setDefence(int d)
    {
        this.defence = d;
    }
    
      /**
     * Set the character's damage
     * @param d new damage level
     */
    public void setDamage(int d)
    {
        this.damage = d;
    }
    
      /**
     * Set the character's descritpion
     * @param d new description level
     */
    public void setDescription(String d)
    {
        this.description = d;
    }
    
      /**
     * Set the character's life
     * @param l new life level
     */
    public void setLife(int l)
    {
        if (l < 0) {
            this.life = 0;
        }
        else {
            this.life = l;
        }
    }
    
    /**
     * Transforme les données de sauvegarde en objet Monster
     *
     * @param  data        données à transformer
     */
    public void transformIn(Character data)
    {
        this.name = data.getName();
        this.description = data.getDescription();
        this.life = data.getLife();
        this.defence = data.getDefence();
        this.damage = data.getDamage();
    }
}
