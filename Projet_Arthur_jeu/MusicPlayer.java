import java.util.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;
import javazoom.jl.player.advanced.AdvancedPlayer;




/**
 * Joue une musique
 * @author tetsatls
 * @version 23/03/2015
 */
public class MusicPlayer
{
    // The current player. It might be null.
    private AdvancedPlayer player;
    private boolean loop;
    
    /**
     * Constructeur qui initialise un objet MusicPlayer
     */
    public MusicPlayer()
    {
        player = null;
    }
    
    /**
     * Joue un pti bout du fichier 
     * @param filename Le fichier qui doit être joué
     */
    public void playSample(String filename)
    {
        try {
            setupPlayer(filename);
            player.play(500);          
        }
        catch (JavaLayerException e) {
            reportProblem(filename);
        }
      
        finally {
            killPlayer();
        }
    }
    
    /**
     * Joue en boucle un fichier audio
     * @param filename Le fichier qui doit être lu
     */
    public void startPlaying(final String filename)
    {
        try {
         
            Thread playerThread = new Thread() {
                public void run()
                {
                    try {
                        loop = true;
                        while (loop) {                           
                            setupPlayer(filename);
                            player.play(5000);                          
                        }

                    }
                    catch (JavaLayerException e) {
                        reportProblem(filename);
                    }
                    finally {
                        killPlayer();
                    }
                }
            };
            playerThread.start();
        }
        catch (Exception ex) {
            reportProblem(filename);
        }
    }
    
    /**
     * Méthode permettant de stoper la musique
     */
    
    public void stop()
    {
        killPlayer();
        loop = false;
    }
    
    /**
     * Charge le fichier qui doit être lu
     * @param filename Le fichier qui doit être lu
     */
    private void setupPlayer(String filename)
    {
        try {
            InputStream is = getInputStream(filename);
            player = new AdvancedPlayer(is, createAudioDevice());
        }
        catch (IOException e) {
            reportProblem(filename);
            killPlayer();
        }
        catch (JavaLayerException e) {
            reportProblem(filename);
            killPlayer();
        }
    }

    /**
     * Return an InputStream for the given file.
     * @param filename The file to be opened.
     * @throws IOException If the file cannot be opened.
     * @return An input stream for the file.
     */
    private InputStream getInputStream(String filename)
        throws IOException
    {
        return new BufferedInputStream(
                    new FileInputStream(filename));
    }

    /**
     * Create an audio device.
     * @throws JavaLayerException if the device cannot be created.
     * @return An audio device.
     */
    private AudioDevice createAudioDevice() throws JavaLayerException
    {
        return FactoryRegistry.systemRegistry().createAudioDevice();
    }

    /**
     * Terminate the player, if there is one.
     */
    private void killPlayer()
    {
        synchronized (this) {
            if (player != null) {
                player.stop();
                player = null;
            }
        }
    }
    
    /**
     * Report a problem playing the given file.
     * @param filename The file being played.
     */
    private void reportProblem(String filename)
    {
        System.out.println("There was a problem playing: " + filename);
    }

}
