import java.awt.event.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

/**
 * Classe-test GraphicViewTest.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 *
 * Les classes-test sont documentées ici :
 * http://junit.sourceforge.net/javadoc/junit/framework/TestCase.html
 * et sont basées sur le document Š 2002 Robert A. Ballance intitulé
 * "JUnit: Unit Testing Framework".
 *
 * Les objets Test (et TestSuite) sont associés aux classes à tester
 * par la simple relation yyyTest (e.g. qu'un Test de la classe Name.java
 * se nommera NameTest.java); les deux se retrouvent dans le męme paquetage.
 * Les "engagements" (anglais : "fixture") forment un ensemble de conditions
 * qui sont vraies pour chaque méthode Test à exécuter.  Il peut y avoir
 * plus d'une méthode Test dans une classe Test; leur ensemble forme un
 * objet TestSuite.
 * BlueJ découvrira automatiquement (par introspection) les méthodes
 * Test de votre classe Test et générera la TestSuite conséquente.
 * Chaque appel d'une méthode Test sera précédé d'un appel de setUp(),
 * qui réalise les engagements, et suivi d'un appel à tearDown(), qui les
 * détruit.
 */
public class GraphicViewTest
{
    // Définissez ici les variables d'instance nécessaires à vos engagements;
    // Vous pouvez également les saisir automatiquement du présentoir
    // à l'aide du menu contextuel "Présentoir --> Engagements".
    // Notez cependant que ce dernier ne peut saisir les objets primitifs
    // du présentoir (les objets sans constructeur, comme int, float, etc.).
    protected double fValeur1;
    protected double fValeur2;
    
    /**
     * Constructeur de la classe-test GraphicViewTest
     */
    public GraphicViewTest()
    {
    }

    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
        // Initialisez ici vos engagements
        fValeur1= 2.0;
        fValeur2= 3.0;
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }
    
    @Test
    public void drawGameTest()
    {
        
        Hero arthur = new Hero("Arthur", "Arthur, roi des bretons et élu des Dieux.", 350, 30, 55, 1.1);
        arthur.getBag().put(0, new Item("epee"));
        arthur.getBag().put(1, new Item("armure"));
        arthur.getBag().put(2, new Item("bouclier"));
        arthur.getBag().put(3, new Item("couronne"));
        
        HashMap obj = new HashMap();
        Room exterieurGrotte = new Room(" devant l'extérieur de la grotte où se trouve le Graal.");
        Room entreeGrotte = new Room("à l'intérieur de la grotte");
        Room campementTruands = new Room("dans le campement d'atroces truands prêt à  tout pour vous voulez votre or.");
        Room bossCapalus = new Room("dans la pièce où dort le terrible monstre légendaire Capalus. Mesurant 10 métres de haut et possédant des griffes coupant la pierre et étant aussi rapide que le vent, il vous faudra une grande détermination pour en venir à bout !");
        Room campementGobelins = new Room("dans un campement de gobelins... D'une rare violence, il feront tout pour faire couler votre sang.");
        Room couloirSerpent = new Room("dans un couloir jonché de serpent...");
        Room bossGlatissante = new Room("en face de l'immonde bête Glatissante! Cette créature étrange possèdant une tête et un cou de serpent, un corps de léopard, un bassin de lion et les sabots d'un cerf. Face à sa puissante, il faudra savoir rusé.");
        Room dameDuLac = new Room("dans une salle où se trouve une fontaine au milieu. Une dame assise vous y attends...");
        Room salleEnigme = new Room("dans une salle remplit d'un champs de force ne vous permettant pas de sortir... Il vous faudra répondre à trois enigmes pour pouvoir vous en sortir.");
        Room bossGobelin = new Room("dans la salle où se trouve le Roi des gobelins. D'un air répugnant, il distille une odeur perfide dans la salle afin de vous empecher de l'attaquer.");
        Room salleMarchand = new Room("dans la salle du marchand. Acheter, vendre et réparer, vous pouvez ici vous trouver tout ce dont vous avez besoin pour votre quête.");
        Room tresorGobelins = new Room("dans une salle remplit d'or, bijou et autre pierre précieuse. Il s'agit du trésor que les gobelins ont volés aux voyageurs.");
        Room corridorDamnes = new Room("dans le corridor où une chaleur puissante fait fondre la pierre. Cela ne présage rien de bon...");
        Room bossDragon = new Room("dans la salle du légendaire Dragon. Ayant un souffle pouvant terrasser une ville, une peau plus dure que la pierre et des griffes pouvant transpercer n'importe quelle matière, vous ne pouvez faire face à ce dragon sans Excalibur...");
        Room salleTeleportation = new Room("dans un salle vide de personne où se trouve pleins de coffre. De quoi vous permettre de vous rétablir.");
        Room grall = new Room("dans la salle où se trouve le Graal. Mais avant de pouvoir le récupérer les Dieux vous mettes à l'épreuvre. Répondez à cette énigme et vous pourrez le récupérer.");
              
        Room heroRoom = new Room("hero");  
        
        // initialisation des sorties
        exterieurGrotte.setExits(null, entreeGrotte, null, null);
        entreeGrotte.setExits(campementTruands, null, campementGobelins, exterieurGrotte);
        campementTruands.setExits(null, bossCapalus, entreeGrotte, null);
        bossCapalus.setExits(null, null, null, campementTruands);
        campementGobelins.setExits(entreeGrotte, salleEnigme, dameDuLac, couloirSerpent);
        couloirSerpent.setExits(null, campementGobelins, bossGlatissante, null);
        bossGlatissante.setExits(couloirSerpent, dameDuLac, null, null);
        dameDuLac.setExits(campementGobelins, null, null, bossGlatissante);
        salleEnigme.setExits(corridorDamnes, bossGobelin, null, campementGobelins);
        bossGobelin.setExits(null, tresorGobelins, salleMarchand, salleEnigme);
        tresorGobelins.setExits(null, null, null, bossGobelin);
        salleMarchand.setExits(bossGobelin, null, null, null);
        corridorDamnes.setExits(null, bossDragon, salleEnigme, null);
        bossDragon.setExits(grall, salleTeleportation, null, corridorDamnes);
        salleTeleportation.setExits(null, null, null, bossDragon);
        grall.setExits(null, null, bossDragon, null);
        
        heroRoom = exterieurGrotte;  // le héros commence devant la grotte
        exterieurGrotte.setVisited();
        
        
        //Création de champs des rooms
        RoomsField rf = new RoomsField(5, 4);
        rf.setRoom(exterieurGrotte, 0, 1);
        rf.setRoom(entreeGrotte, 1, 1);
        rf.setRoom(campementTruands, 1, 0);
        rf.setRoom(bossCapalus, 2, 0);
        rf.setRoom(campementGobelins, 1, 2);
        rf.setRoom(couloirSerpent, 0, 2);
        rf.setRoom(bossGlatissante, 0, 3);
        rf.setRoom(dameDuLac, 1, 3);
        rf.setRoom(salleEnigme, 2, 2);
        rf.setRoom(bossGobelin, 3, 2);
        rf.setRoom(tresorGobelins, 4, 2);
        rf.setRoom(salleMarchand, 3, 3);
        rf.setRoom(corridorDamnes, 2, 1);
        rf.setRoom(bossDragon, 3, 1);
        rf.setRoom(salleTeleportation, 4, 1);
        rf.setRoom(grall, 3, 0);
                
        for (int y = 0; y < 4; y++){
            for (int x = 0; x < 5; x++){
                if(rf.getRoom(x,y)!=null)
                    rf.getRoom(x,y).setVisited();
            }
        }
        
        GraphicView graphicView = new GraphicView();        
        graphicView.showStatus(rf, heroRoom, arthur, "");    
    }
    
    @Test
    public void drawAllTest()
    {
        HashMap objects = new HashMap();
        Hero hero = new Hero("Arthur", "Hero", 50, 80, 10, 13.0);
        
        Room testRoom = new Room("testRoom");        
        Room heroRoom = new Room("hero"); 
        testRoom.setVisited();  
        
        // Room north, Room east, Room south, Room west
        testRoom.setExits(  new Room("default"), new Room("default"),
                            new Room("default"), new Room("default")  );
        
        RoomsField roomsField = new RoomsField(7, 7);
        for (int x = 0; x < 7; x++){
            for (int y = 0; y < 7; y++){
                roomsField.setRoom(testRoom, x, y);
                if(x==5 && y==5)
                    heroRoom=testRoom;
            }
        }
                
        GraphicView graphicView = new GraphicView();        
        graphicView.showStatus(roomsField, heroRoom, hero, "west");
    }
    
    @Test
    public void showStatusTest()
    {
        HashMap objects = new HashMap();
        Hero hero = new Hero("Arthur", "Hero", 50, 80, 10, 13.0);
        
        Room testRoom1 = new Room("testRoom1");
        Room testRoom2 = new Room("testRoom2");  
        Room testRoom3 = new Room("testRoom3");  
        Room testRoom4 = new Room("testRoom4");  
        Room testRoom5 = new Room("testRoom5");  
        Room heroRoom = new Room("heroRoom");
        
        testRoom1.setVisited(); 
        testRoom2.setVisited(); 
        testRoom3.setVisited(); 
        testRoom4.setVisited(); 
        testRoom5.setVisited(); 
        
        // Room north, Room east, Room south, Room west
        testRoom1.setExits(testRoom2, testRoom5, testRoom4, testRoom3);
        testRoom2.setExits(null, null, testRoom1, null);
        testRoom3.setExits(null, testRoom1, null, null);
        testRoom4.setExits(testRoom1, null, null, null);
        testRoom5.setExits(null, null, null, testRoom1);
        
        RoomsField roomsField = new RoomsField(5, 5);
        for (int y = 0; y < 4; y++){
            for (int x = 0; x < 5; x++){
                roomsField.setRoom(new Room("default"), x, y);
            }
        }
                
        roomsField.setRoom(testRoom1, 3, 3);        
        roomsField.setRoom(testRoom2, 3, 2);
        roomsField.setRoom(testRoom3, 2, 3);
        roomsField.setRoom(testRoom4, 3, 4);
        roomsField.setRoom(testRoom5, 4, 3);
        
        GraphicView graphicView = new GraphicView();
        heroRoom = testRoom2;
        graphicView.showStatus(roomsField, heroRoom, hero, "north");
        
        heroRoom = testRoom3;
        graphicView.showStatus(roomsField, heroRoom, hero, "east");
        
        heroRoom = testRoom4;
        graphicView.showStatus(roomsField, heroRoom, hero, "south");
        
        heroRoom = testRoom5;
        graphicView.showStatus(roomsField, heroRoom, hero, "west");
    }
}