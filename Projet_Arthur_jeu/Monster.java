import java.util.*;

/**
 * Write a description of class Monster here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Monster extends Character
{
    private final static int sort =2 ;

    /**
     * Constructor for objects of class Monster
     */
    public Monster(String n, String d, int pv, int defense, int damage){
        super(n,d,pv,defense,damage);
        
    }

    /**
     * Méthode permattant d'attaquer l'héro
     * 
     * @param  h   héro
     */
    public String attack(Hero h)
    {
        h.setLife(h.getLife()-(getDamage()-h.getDefence()));
        
        if(h.getLife()<0){
            return " Vous êtes mort";
        }else{ 
            return "Il vous reste" +h.getLife();
        }
    }
    
     /**
     * Méthode permattant de lancer un sort sur l'héro
     * Les sort permettent d'infliger 2fois plus de degât que les attaques de base mais fait subir également des dégât. 
     * 
     * @param  h   héro
     */
    public String fate(Hero h)
    {
        
        h.setLife(h.getLife()-(getDamage()*sort -h.getDefence()));
        setLife(getLife()-20);
        
        if(h.getLife()<0){
            return " Vous êtes mort";
        }else{ 
            return "Il vous reste" +h.getLife();
        }
    }
    
    
     /**
     * Méthode permattant de voler un objet à l'héro
     * 
     * @param  h   héro
     */
    public void rob(Hero h)
    {
       HashMap<Integer, Object> sac = h.getBag();
       for (int i = 4; i<10; i++){ //On vérifie tout les entrées du sac
                
                if(sac.get(i) instanceof Potion){
                    h.getBag().put(i, null);
                    
                }
            }
        }
    
    /**
     * Méthode pour accéder au sort
     * 
     * 
     */
    public int getSort()
    {
        // put your code here
        return sort;
        
    }
    
    /**
     * Méthode pour ce défendre
     * 
     * 
     */
    public void defend(Hero h)
    {
        if(getDefence()+5 < h.getDamage()){
            this.setDefence(getDefence()+5);
        }
        
    }

    
}

