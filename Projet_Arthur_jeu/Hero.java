import java.util.*;
    
/**
 * Write a description of class Hero here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Hero extends Character 
{
    // instance variables - replace the example below with your own
    private HashMap <Integer, Object> bag;
    private double chance;
    

    /**
     * Constructor for objects of class Hero
     */
    public Hero(String n, String d, int pv, int defense,int damage, double chance){
        super(n,d,pv,defense,damage);
        this.chance=chance;
        bag= new HashMap<Integer,Object>();
        
    }
    
        /**
     * Getteur pour le bag du héro
     * @return bag    il s'agit du sac du héros où se trouve son équipement et ses potions
     */
    public HashMap<Integer, Object> getBag() {
        return bag;
    }

    /**
     * Setteur pour le bag du héro
     * @param item représente une potion
     */
    public void addBag(Object item){
        int indice = 4;
        for (Integer mapKey : bag.keySet()) {         
            if(bag.get(mapKey) == null){
                indice = mapKey;
            }    
        }
        bag.put(indice ,item);
    }
    
    /**
     * Affichage de ce qui se situe dans le sac du héros
     */
    public String afficherBag(){
        String toStringBag = "";
        Set<Integer> keys = bag.keySet();
          for (Integer key : keys) {         
            if(bag.get(key) != null){
                toStringBag += ((Item)bag.get(key)).getName();
                System.out.println();
            }    
        }
        return toStringBag;
    }
    
    public void transformIn(Hero data){

        this.setName(data.getName());
        this.setDescription(data.getDescription());
        this.setLife(data.getLife());
        this.setDefence(data.getDefence());
        this.setDamage(data.getDamage());
        this.setChance(data.getChance());
        
        // On commence à partir de 4 car les équipements avant n'évoluent pas et son disponible dès le début du jeu 
        for(int i = 4; i<10; i++){
            Object potion = (Object)data.getBag().get(i);
            this.bag.put(i, potion);
        }
    }
    
      /**
     * Get the character's dodge
     * 
     * return the dodge of the character
     */
    public double getChance()
    {
        return this.chance;
    }
    
        /**
     * Set the character's dodge
     */
    public void setChance(double c)
    {
        this.chance=c;
    }
}