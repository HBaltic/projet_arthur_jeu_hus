import java.util.*;
import java.io.*;
/**
 *  Cette classe est la classe principale(main)du jeu "Arthur, la conquête du Graal". 
 *  "Arthur, la conquête du Graal" est un jeu d'aventure textuel. 
 *  Vous pouvez ainsi découvrir une quainzaine d'endroits, 
 *  vous battre contre des monstres légendaires et répondre à différentes énigmes.
 * 
 *  Pour jouer au jeu il suffit d'instancier la classe Game et d'appeler la méthode "plays()".
 * 
 *  La classe Game crée et initialise toutes les rooms, le parser(analyseur) et lance le jeu. Elle permet aussi
 *  d'éxecuter et d'évaluer les commandes que le parser retourne.
 * 
 * @author  Husref BALTIC
 * @version 1.0
 */

public class Game 
{
    private Parser parser;
    private Room currentRoom;
    private RoomsField rf;
    private Hero arthur;
    private HashMap<Integer, Object> obj = new HashMap<Integer, Object>();
    private Monster monster;
    private boolean modeFight; //boolean permettant de passer les commandes en mode combat
    private MusicPlayer player;
    private GraphicView gv;
    private boolean finished;
        
    /**
     * Constructeur permettant de créer le jeu et d'initialiser la carte.
     */
    public Game() 
    {
        createMap();
    }

    /**
     * Méthode permettant de créer toutes les rooms et les relier entre elles, les items 
     * et les différents characters dans les rooms.
     */
    private void createMap()
    {   
        //Initialisation des gobelins
        Monster sesterce = new Monster("Sesterce le gobelin", "Sesterce  est un gobelin avide de sang et d'argent.", 150, 40, 50);
        Monster ket = new Monster("Ket le gobelin", "Ket est un gobelin de bas étage possèdant une certaine force.", 100, 30, 50);
        Monster gakunr = new Monster("Gakunr le gobelin", "Gakunr est un gobelin-chamane possèdant une sorcellerie puissance.", 200, 20, 60);
        Monster smab = new Monster("Smab le gobelin", "Smab est un gobelin-guetteur qui possède une vision thermique.", 150, 40, 50);
        Monster zerfall = new Monster("Zerfall le gobelin", "Zerfall est un gobelin accompagnant Sersterce dans ces traquenards.", 150, 40, 50);
        Monster unomalith = new Monster("Unomalith le gobelin", "Unomalith est un gobelin-voleur aussi rapide que le vent.", 150, 40, 55);
        Monster ibaregan = new Monster("Ibaregan le gobelin", "Ibaregan est un gobelin-chamane pratiquant une magie noir très puissante.", 200, 35, 55);
        Monster araytha = new Monster("Araytha la gobelin", "Araytha est une femme gobelin attirant les hommes vers un destin funèbre...", 100, 30, 50);
        Monster saytha = new Monster("Saytha la gobelin", "Saytha est une femme gobelin petite et vicieuse.", 150, 50, 50);
        Monster iberamond = new Monster("Iberamond le gobelin", "Iberamond est un gobelin de garde protègeant les trésors de ces menbres.", 250, 50, 60);
        Monster vilawyr = new Monster("Vilawyr le gobelin", "Vilawyr fait 3 mètres de haut et mange tout ce qui lui passe sous la mains, y compris les gobelins.", 200, 30, 70);
        Monster corenydd = new Monster("Corenydd le gobelin", "Corenydd est un gobelin des montagne pouvant résister à de puissantes attaques.", 200, 55, 45);
        Monster tietram = new Monster("Titetram le gobelin", "Titetram aime rendre visite aux habitants des villages voisins le soir, afin de leur voler leurs argents.", 150, 40, 50);
        Monster astarin = new Monster("Astarin le gobelin", "Astarin mi-elfe mi-gobelin, il possède toutes les qualités de l'un et tout les inconvenients de l'autre.", 100, 40, 60);
        Monster parainor = new Monster("Parainor le gobelin", "Parainor jouie d'une position surpérieur par rapport aux autres de part son lien de parenté avec le roi des gobelin.", 250, 30, 60);
        Monster leaseth = new Monster("Leaseth la gobelin", "Leaseth est une femme gobelin, ancienne épouse du roi.", 200, 40, 55);
        Monster ocoared = new Monster("Ocoared le gobelin", "Ocoared est un musicien à ces heures perdues qui aime construire ces instrumets avec le corps de ces victimes.", 100, 30, 50);
        Monster cadalesean = new Monster("Cadalesean le gobelin", "Cadalesean est le compagnon de route de Corenydd.", 200, 50, 40);
        Monster dwaybard = new Monster("Dwaybard le gobelin", "Dwaybard est un des gardes qui protège le roi des gobelins.", 350, 40, 60);
        Monster ethietrem = new Monster("Ethietram le gobelin", "Ethietram est le second garde qui protège le roi des gobelins.", 250, 40, 60);
        
        //Initialisation des truands
        Monster vallec = new Monster("Vallec le truand", "Vallec est un truand de bas étage qui vit du vol.", 100, 30, 50);
        Monster badeles = new Monster("Badeles le truand", "Badeles a laissé femme et enfant pour s'adonner à la truanderie.", 150, 40, 55);
        Monster galin = new Monster("Galin le truand", "Galin est une ancienne gloire des truands, notamment grâce à ces faits d'armes.", 200, 30, 60);
        Monster druris = new Monster("Druris le truand", "Druris vole aux pauvres et donne aux riches.", 200, 40, 50);
        Monster oraret = new Monster("Oraret le truand", "Oraret possèdent des pouvoirs lui permettant de voler dans son sommeil.", 150, 35, 55);
        Monster gingrain = new Monster("Gingrain le truand", "Gingrain est un truand séduisant ces victimes avant de les assassiner.", 150, 35, 60);
        Monster galedon = new Monster("Galedon le truand", "Galedon est un truand qui sevit dans plusieurs vllages aux alentours.", 200, 40, 55);
        Monster bandelois = new Monster("Bandelois le truand", "Bandelois est un truand-noble, il aurait fait partie de la bourgeoisie dans une autre vie.", 200, 40, 50);
        Monster ferholt = new Monster("Ferholt le truand", "Ferholt est un truand possèdant une grande habilité dans le vole.", 250, 40, 55);
        Monster gingwys = new Monster("Gingwys le truand", "Gingwys est un truand possèdant plusieurs esclaves qu'il torture à longueur de journée.", 250, 40, 60);
        Monster dragover = new Monster("Dragover le truand", "Dragover aurait vaincu un dragon à lui tout seul...", 250, 40, 60);
        Monster lararan = new Monster("Lararan le truand", "Lararan est un ancien pirate voulant se reconvertir car il vait le mal de mer.", 150, 35, 55);
        Monster meriagros = new Monster("Meriagros le truand", "Meriagros est l'un des grands combattants.", 300, 35, 60);
        Monster celimart = new Monster("Celimart le truand", "Celimart aurait été les des gardes personnels d'Uther Pendragon.", 250, 40, 60);
        Monster girgan = new Monster("Girgan le truand", "Girgan est un truand vivant de la vente d'alcool illégal.", 150, 35, 55);
        Monster sagrehus = new Monster("Sagrehus le truand", "Sagrehus est un des plus fort combattant du campement des turands.", 300, 45, 60);
        Monster bruris = new Monster("Bruris le truand", "Bruris est le compagnon de Vallec.", 150, 35, 50);
        Monster bermagus = new Monster("Bermagus le truand", "Bermagus est un truand possèdant des pouvoirs celeste...", 250, 40, 55);
        Monster elistan = new Monster("Elistan le truand", "Elistan est une femme truand séduisant ces ennemies avant de les tuer.", 150, 35, 60);
        Monster pedinas = new Monster("Pedinas le truand", "Pedinas est un truand gardant l'entrée du campement.", 200, 40, 55);
        
        //Initialisation des Potions
        Potion pElixir1 = new Potion ("Elixir+", 10);
        Potion pElixir2 = new Potion ("Elixir+", 10);
        Potion pElixir3 = new Potion ("Elixir+", 10);
        Potion pElixir4 = new Potion ("Elixir++", 20);
        Potion pElixir5 = new Potion ("Elixir++", 20);
        Potion pElixir6 = new Potion ("Elixir++", 20);
        Potion pElixir7 = new Potion ("Elixir+++", 50);
        Potion pElixir8 = new Potion ("Elixir+++", 50);
        Potion pChance1 = new Potion ("Chance+", 1);
        Potion pChance2 = new Potion ("Chance+", 1);
        Potion pChance3 = new Potion ("Chance+", 1);
        Potion pChance4 = new Potion ("Chance++", 2);
        Potion pChance5 = new Potion ("Chance++", 2);
        Potion pChance6 = new Potion ("Chance++", 2);
        Potion pChance7 = new Potion ("Chance+++", 3);
        Potion pChance8 = new Potion ("Chance+++", 3);
        
        //Initialisation des pièges
        Trap tPoison1 = new Trap ("Poison+", 10);
        Trap tPoison2 = new Trap ("Poison+", 10);
        Trap tPoison3 = new Trap ("Poison+", 10);
        Trap tPoison4 = new Trap ("Poison++", 25);
        Trap tPoison5 = new Trap ("Poison++", 25);
        Trap tPoison6 = new Trap ("Poison++", 25);
        Trap tPoison7 = new Trap ("Poison+++", 50);
        Trap tPoison8 = new Trap ("Poison+++", 50);
        Trap tChance1 = new Trap ("PoisonChance+", 1);
        Trap tChance2 = new Trap ("PoisonChance+", 1);
        Trap tChance3 = new Trap ("PoisonChance+", 1);
        Trap tChance4 = new Trap ("PoisonChance++", 2);
        Trap tChance5 = new Trap ("PoisonChance++", 2);
        Trap tChance6 = new Trap ("PoisonChance+++", 3);
        
        //Ajout à attribut obj
        obj.put(1, pElixir4);        
        obj.put(2, tPoison5);
        obj.put(3, pElixir1);
        obj.put(4, tPoison2);
        obj.put(5, pChance1);
        obj.put(6, tChance4);
        obj.put(7, vallec);
        obj.put(8, badeles);
        obj.put(9, galin);
        obj.put(10, sesterce);
        obj.put(11, pElixir2);
        obj.put(12, tPoison4);
        obj.put(13, tChance2);
        obj.put(14, tPoison2);
        obj.put(15, ket);
        obj.put(16, gakunr);
        obj.put(17, smab);
        obj.put(18, druris);
        obj.put(19, zerfall);
        obj.put(20, tChance4);
        obj.put(21, pElixir3);        
        obj.put(22, pChance5);
        obj.put(23, unomalith);
        obj.put(24, oraret);
        obj.put(25, gingrain);
        obj.put(26, ibaregan);
        obj.put(27, tPoison3);
        obj.put(28, tChance3);
        obj.put(29, tPoison8);
        obj.put(30, tChance1);
        obj.put(31, galedon);
        obj.put(32, araytha);
        obj.put(33, saytha);
        obj.put(34, bandelois);
        obj.put(35, pElixir7);
        obj.put(36, pChance6);
        obj.put(37, tChance1);
        obj.put(38, tPoison1);
        obj.put(39, ferholt);
        obj.put(40, iberamond);
        obj.put(50, gingwys);
        obj.put(51, vilawyr);        
        obj.put(52, pChance4);
        obj.put(53, tChance5);
        obj.put(54, pElixir8);
        obj.put(55, tPoison7);
        obj.put(56, pChance2);
        obj.put(57, pChance2);
        obj.put(58, corenydd);
        obj.put(59, dragover);
        obj.put(60, lararan);
        obj.put(61, tietram);
        obj.put(62, tPoison8);
        obj.put(63, pChance7);
        obj.put(64, tChance6);
        obj.put(65, pElixir5);
        obj.put(66, pChance6);
        obj.put(67, meriagros);
        obj.put(68, pElixir6);
        obj.put(69, pChance8);
        obj.put(70, astarin);
        
        //Pour campement des gobelins
        obj.put(91, celimart);
        obj.put(92, girgan);
        obj.put(93, sagrehus);
        obj.put(94, bruris);
        obj.put(95, bermagus);
        obj.put(96, elistan);
        
        //Pour campement des truands
        obj.put(97, parainor);
        obj.put(98, leaseth);
        obj.put(99, ocoared);
        obj.put(100, cadalesean);
        obj.put(101, dwaybard);
        obj.put(102, ethietrem);
        
        
        //Initialisation des Boss
        Monster capalus = new Monster("Capalus le noir", "C'est monstre légendaire mesurant 10 métres de haut,possédant des griffes coupant la pierre et étant aussi rapide que le vent.", 400, 50, 65);
        Monster glatissante = new Monster("La bête Glatissante", "C'est un monstre possèdant une tête de serpent lui permettant d'attaquer mortellement, un corps de félin pour agir et se déplacer rapidement et des sabots de cerf qui lui permette de faire trembler la terre.", 500, 55, 60); 
        Monster roiGobelin = new Monster("Le roi des Gobelin", "Ce monstre possède une force surnaturel avec sa masse géant frabriqué par les ossements de ces victimes...", 600, 50, 65);
        Monster dragon = new Monster("Le légendaire Dragon", "Un monstre venue des bas fond de la terre, fils de satan et élue du monde des ténèbres... Ce monstre est votre dernier obstacle avant la réussite de votre quête.", 100, 30, 55);
            
        //Pour salle des boss
        obj.put(105, capalus);
        obj.put(106, glatissante);
        obj.put(107, roiGobelin);
        obj.put(108, dragon );
        
        //initialisation des équipements
        Gear epee = new Gear("Excalibur", 10, 0, 1);
        Gear armure = new Gear("Armure étincelante", 0, 5, 1);
        Gear bouclier = new Gear("Bouclier étincelant", 0, 7, 1);
        Gear couronne = new Gear("Couronne de Camelot", 0, 0, 5);
        
        //initialisation du Héros
        arthur = new Hero("Arthur", "Arthur, roi des bretons et élu des Dieux.", 350, 30, 55, 1.1);
        
        //Ajout de l'équipement du héro
        
        arthur.getBag().put(0, epee);
        arthur.getBag().put(1, armure);
        arthur.getBag().put(2, bouclier);
        arthur.getBag().put(3, couronne);
        arthur.getBag().put(4, pElixir1);
        // création des rooms
        Room exterieurGrotte = new Room(" devant l'extérieur de la grotte où se trouve le Graal.");
        Room entreeGrotte = new Room("à l'intérieur de la grotte");
        Room campementTruands = new Room("dans le campement d'atroces truands prêt à  tout pour vous voulez votre or.");
        Room bossCapalus = new Room("dans la pièce où dort le terrible monstre légendaire Capalus. Mesurant 10 métres de haut et possédant des griffes coupant la pierre et étant aussi rapide que le vent, il vous faudra une grande détermination pour en venir à bout !");
        Room campementGobelins = new Room("dans un campement de gobelins... D'une rare violence, il feront tout pour faire couler votre sang.");
        Room couloirSerpent = new Room("dans un couloir jonché de serpent...");
        Room bossGlatissante = new Room("en face de l'immonde bête Glatissante! Cette créature étrange possèdant une tête et un cou de serpent, un corps de léopard, un bassin de lion et les sabots d'un cerf. Face à sa puissante, il faudra savoir rusé.");
        Room salleLancelot = new Room("dans une salle où se trouve un viel ami à vous...");
        Room salleEnigme = new Room("dans une salle remplit d'un champs de force ne vous permettant pas de sortir... Il vous faudra répondre à trois enigmes pour pouvoir vous en sortir.");
        Room bossGobelin = new Room("dans la salle où se trouve le Roi des gobelins. D'un air répugnant, il distille une odeur perfide dans la salle afin de vous empecher de l'attaquer.");
        Room salleMarchand = new Room("dans la salle du marchand. Acheter, vendre et réparer, vous pouvez ici vous trouver tout ce dont vous avez besoin pour votre quête.");
        Room tresorGobelins = new Room("dans une salle remplit d'or, bijou et autre pierre précieuse. Il s'agit du trésor que les gobelins ont volés aux voyageurs.");
        Room corridorDamnes = new Room("dans le corridor où une chaleur puissante fait fondre la pierre. Cela ne présage rien de bon...");
        Room bossDragon = new Room("dans la salle du légendaire Dragon. Ayant un souffle pouvant terrasser une ville, une peau plus dure que la pierre et des griffes pouvant transpercer n'importe quelle matière, vous ne pouvez faire face à ce dragon sans Excalibur...");
        Room salleTeleportation = new Room("dans un salle vide de personne où se trouve pleins de coffre. De quoi vous permettre de vous rétablir.");
        Room grall = new Room("dans la salle où se trouve le Graal. Mais avant de pouvoir le récupérer les Dieux vous mettes à l'épreuvre. Répondez à cette énigme et vous pourrez le récupérer.");
        
         // initialisation des sorties
        exterieurGrotte.setExits(null, entreeGrotte, null, null);
        entreeGrotte.setExits(campementTruands, null, campementGobelins, exterieurGrotte);
        campementTruands.setExits(null, bossCapalus, entreeGrotte, null);
        bossCapalus.setExits(null, null, null, campementTruands);
        campementGobelins.setExits(entreeGrotte, salleEnigme, salleLancelot, couloirSerpent);
        couloirSerpent.setExits(null, campementGobelins, bossGlatissante, null);
        bossGlatissante.setExits(couloirSerpent, salleLancelot, null, null);
        salleLancelot.setExits(campementGobelins, null, null, bossGlatissante);
        salleEnigme.setExits(corridorDamnes, bossGobelin, null, campementGobelins);
        bossGobelin.setExits(null, tresorGobelins, salleMarchand, salleEnigme);
        tresorGobelins.setExits(null, null, null, bossGobelin);
        salleMarchand.setExits(bossGobelin, null, null, null);
        corridorDamnes.setExits(null, bossDragon, salleEnigme, null);
        bossDragon.setExits(grall, salleTeleportation, null, corridorDamnes);
        salleTeleportation.setExits(null, null, null, bossDragon);
        grall.setExits(null, null, bossDragon, null);

       currentRoom = exterieurGrotte;  // on commence devant la grotte
       exterieurGrotte.setVisited();
        
        //Création de champs des rooms
        rf = new RoomsField(10, 10);
        rf.clear();
        rf.setRoom(exterieurGrotte, 0, 1);
        rf.setRoom(entreeGrotte, 1, 1);
        rf.setRoom(campementTruands, 1, 0);
        rf.setRoom(bossCapalus, 2, 0);
        rf.setRoom(campementGobelins, 1, 2);
        rf.setRoom(couloirSerpent, 0, 2);
        rf.setRoom(bossGlatissante, 0, 3);
        rf.setRoom(salleLancelot, 1, 3);
        rf.setRoom(salleEnigme, 2, 2);
        rf.setRoom(bossGobelin, 3, 2);
        rf.setRoom(tresorGobelins, 4, 2);
        rf.setRoom(salleMarchand, 3, 3);
        rf.setRoom(corridorDamnes, 2, 1);
        rf.setRoom(bossDragon, 3, 1);
        rf.setRoom(salleTeleportation, 4, 1);
        rf.setRoom(grall, 3, 0);
        
       //Instanciation music player
       player=new MusicPlayer();
       
       //Instanciation de l'analyseur
       parser = new Parser();
       
       //Instanciation de la partie graphique
       gv = new GraphicView();
       
       gv.showStatus(rf, currentRoom, arthur, null);
    }

    /**
     *  Méthode permettant de lancer le jeu et boucle tant que le jeu n'est pas fini.
     */
    public void play() 
    {   
        player.startPlaying("audio/Medieval-Theme.mp3");
        System.out.println("Bienvenue Sur 'Arthur, la conquête du Graal.' ");
        System.out.println("Si vous souhaitez jouer une nouvelle partie, tapez juste sur la touche Entrer.");
        System.out.println("Si vous avez déjà une partie en cours, tapez 'load' au clavier.");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        if(str.equals("load")){
            try{
                        loader();
                        gv.showStatus(rf, currentRoom, arthur, null);
                    }catch (FileNotFoundException e){
                        //T0D0 Auto-generated catch block
                        e.printStackTrace();
                    }catch (ClassNotFoundException e) {
                        //T0D0 Autogenerated catch block
                        e.printStackTrace();
                    }catch (IOException e) {
                        //T0D0 Autogenerated catch block
                        e.printStackTrace();
                    } 
            
        }else if(str.equals("")){
            System.out.println();
            System.out.print("Vous incarnez le légendaire Arthur ");
            System.out.println("lors de sa poursuite du Graal.");
            System.out.println("Vous êtes accompagné de Merlin, qui usera de ses pouvoirs magiques afin de vous aider dans votre conquête.");
            System.out.println(" Tapez 'merlin' afin qu'il vous viennes en aide.");
            System.out.println("Vous pouvez sauvegarder votre partie en tapant 'save'.");
            System.out.println("A vous de jouer !");
            printSituation();
        }
        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
             
        finished = false;
        while (!finished) {
            Command command = parser.getCommand();
            if(finished = processCommand(command) || finished){
                finished = true;
            }
        }
        player.stop();
        System.out.println("Merci d'avoir jouer et à la prochaine pour de nouvelles aventures !");
    }

    /**
     * Méthode affichant un message de bienvenue lors d'un lancement de partie.
     */
    private void printSituation()
    {
        System.out.println("Vous êtes actuellement " + currentRoom.getDescription());
        System.out.print("Voici les différentes sorties : ");
        if (currentRoom.getNorthExit() != null) {
            System.out.print("Nord ");
        }
        if (currentRoom.getEastExit() != null) {
            System.out.print("Est ");
        }
        if (currentRoom.getSouthExit() != null) {
            System.out.print("Sud ");
        }
        if (currentRoom.getWestExit() != null) {
            System.out.print("Ouest ");
        }
        System.out.println();
    }

    /**
     * Méthode recevant une commande en paramêtre et retourne l'ordre voulu
     * @param command La commande à analyser.
     * @return true si la commande fini le jeu, sinon retourne false.
     */
    private boolean processCommand(Command command) 
    {
     boolean wantToQuit = false;
    
        //Commande non reconnue
        if (command.isUnknown()) {
            System.out.println("Je ne comprends pas ce que vous voulez dire...");
            return false;
        }
        
        //Commande merlin(help) toujours disponoble à n'importe quel moment du jeu
        String commandWord = command.getCommand();
        if (commandWord.equals("merlin")) {
            printMerlin();
        }
        
        //Choix du mode de commande en fonction de la situation 
        if(modeFight)
        {
            commandWord = command.getCommand();
            if (commandWord.equals("att")) {
                attackEnnemy();
            }
            else if (commandWord.equals("def")) {
                defend();
            }
            else if (commandWord.equals("drink")) {
                 drink(command);
                 
            }else if (commandWord.equals("esc")) {
                escape();
            }    
        }else{
                if (commandWord.equals("go")) {
                    goRoom(command);
                }
                else if (commandWord.equals("quit")) {
                    wantToQuit = quit(command);
                }else if (commandWord.equals("search")) {
                    searchRoom(command);
                }else if (commandWord.equals("save")) {
                    try{
                        saving();
                    }catch (FileNotFoundException e){
                        //T0D0 Auto-generated catch block
                        e.printStackTrace();
                    }catch (IOException e) {
                        //T0D0 Autogenerated catch block
                        e.printStackTrace();
                    }
                }else if (commandWord.equals("load")) {
                    try{
                        loader();
                    }catch (FileNotFoundException e){
                        //T0D0 Auto-generated catch block
                        e.printStackTrace();
                    }catch (ClassNotFoundException e) {
                        //T0D0 Autogenerated catch block
                        e.printStackTrace();
                    }catch (IOException e) {
                        //T0D0 Autogenerated catch block
                        e.printStackTrace();
                    } 
                }
            }
     
        return wantToQuit;
   }

    // Implémentation des commandes de l'utilisateur:

    /**
     *Méthode permettant d'avoir de l'aide de la par de l'enchenteur Merlin.
     */
    private void printMerlin() 
     {
        //Aide lors du mode fight(attaquer, defendre, boire)
        if(modeFight)
        {
            System.out.println("Attention mon roi un ennemi vous a tendu un piège!");
            System.out.println("Vous êtes actuellement en mode combat, vos actions disponibles ont donc changées!");
            System.out.println();
            System.out.println("Pour attaquer l'adversaire utiliser la commande 'att';");
            System.out.println("Pour vous défendre d'une attaque utiliser 'def';");
            System.out.println("Afin de regagner des points de vie prennait une potion avec 'drink' suivit du type de la potion(ex: Elixir+);");
            System.out.println("Si vous souhaitez ne pas combattre, essayer de fuire avec 'esc';");
            System.out.println();
            System.out.println(monster.getName() + " se met sur votre chemin, il va falloir le combattre !");
            
             
        }else{
        
        //Aide lors du mode normal(bouger, fouiller, quitter)
        System.out.println("Vous avez l'air perdu mon roi...");
        System.out.println("Vous êtes actuellement " + currentRoom.getDescription());
        System.out.println();
        if (currentRoom.getNorthExit() != null) {
            System.out.println("La sortie au Nord de cette pièce vous ménera " + currentRoom.getNorthExit().getDescription());
           
        }
        if (currentRoom.getEastExit() != null) {
            System.out.println("La sortie à  l'Est de cette pièce vous ménera " + currentRoom.getEastExit().getDescription());
        }
        if (currentRoom.getSouthExit() != null) {
            System.out.println("La sortie au Sud de cette pièce vous ménera " + currentRoom.getSouthExit().getDescription());
        }
        if (currentRoom.getWestExit() != null) {
            System.out.println("La sortie à  l'Ouest de cette pièce vous ménera " + currentRoom.getWestExit().getDescription());
        }
        System.out.println();
        System.out.println("Vous pouvez vous déplacer avec la commande 'go'"
                            + ", quitter le jeu avec  'quit' et me demander de "
                            + "l'aide en tapant 'merlin'.");
        }
    }

    /** 
     * Méthode permettant d'avancer dans le jeu en changeant de rooms.
     * @param command La commande à analyser.
     */
    private void goRoom(Command command) 
    {
        if (!command.hasSecondWord()) {
            // Si il n'y a pas de second mot...
            System.out.println("Vers où ?");
            return;
        }

        String direction = command.getSecondWord();

        //Essaye de quitter la room où l'on se trouve
        Room nextRoom = null;
        if (direction.equals("nord")) {
            gv.showStatus(rf, currentRoom, arthur, "nord"); // Permet de récupérer la direction prise par le joueur
            nextRoom = currentRoom.getNorthExit();          
        }
        if (direction.equals("est")) {
            gv.showStatus(rf, currentRoom, arthur, "est");
            nextRoom = currentRoom.getEastExit();         
        }
        if (direction.equals("sud")) {
            gv.showStatus(rf, currentRoom, arthur, "sud");
            nextRoom = currentRoom.getSouthExit();
        }
        if (direction.equals("ouest")) {
            gv.showStatus(rf, currentRoom, arthur, "ouest");
            nextRoom = currentRoom.getWestExit();
        }

        if (nextRoom == null) {
            System.out.println("Il vous est impossible de passer par là Sir!");
        }
        else {
            currentRoom = nextRoom;
            System.out.println("Vous êtes actuellement " + currentRoom.getDescription());
            
            
            if(currentRoom == rf.getRoom(1, 3)){
               // Salle de lancelot, il va vous donner plus de défense et d'attaque
               if(!currentRoom.getVisited()){
                   System.out.println();
                   System.out.println("Bonjour mon roi, je vous ai rejoins ici afin de vous aider dans votre quête et vous donner plus de force afin de vaincre les monstre qui se mainteront sur votre chemin");
                   System.out.println("Lancelot vous augmente votre attaque de 10!");
                   arthur.setDamage(arthur.getDamage() + 10);
                   System.out.println("Lancelot vous augmente votre defense de 10!");
                   arthur.setDefence(arthur.getDefence() + 10);
                   System.out.println("Lancelot vous donne un Elixir++!");
                   Potion pE = new Potion ("Elixir++", 20);
                   arthur.addBag(pE);
                   System.out.println();
                   System.out.println("J'espère que cela sera suffisant pour la réussite de votre quête Sir... A bientôt!");
               }else{
                 System.out.println("Je n'ai plus rien à vous donner Sir, passer votre chemin...");  
               }
            }else if(currentRoom == rf.getRoom(2, 0) ){
                if(!currentRoom.getVisited()){
                    //On vérifie si la nouvelle room correspond à l'une des rooms d'un boss
                    // Si oui appelle RandomizeObject qui va se charger de trouver le bon boss correspondant a la room actuelle + les indices données servant au code secret de la fin du jeu
                    randomizeObject();
                    if(monster.getLife() == 0){ //Vérification si le joueur à gagner le combat ou non
                        System.out.println();
                        System.out.println("Le monstre vous a donné un indice : <<..er..>>");
                        System.out.println("Il vaut mieux conserver cela dans un coin, il pourrait nous servir.");
                    }
                }           
            }else if(currentRoom == rf.getRoom(0, 3)){
                if(!currentRoom.getVisited()){
                    randomizeObject();
                    if(monster.getLife() == 0){
                        System.out.println();
                        System.out.println("Le monstre vous a donné un indice : <<...dragon>>");
                        System.out.println("Il vaut mieux conserver cela dans un coin, il pourrait nous servir.");
                    }
                }
            }else if( currentRoom == rf.getRoom(3, 2)){
                if(!currentRoom.getVisited()){
                    randomizeObject();
                    if(monster.getLife() == 0){
                        System.out.println();
                        System.out.println("Le monstre vous a donné un indice : <<Pen...>>");
                        System.out.println("Il vaut mieux conserver cela dans un coin, il pourrait nous servir.");
                    }
                }
            }else if(currentRoom == rf.getRoom(3, 1)){
                if(!currentRoom.getVisited()){
                    randomizeObject();
                    if(monster.getLife() == 0){
                        System.out.println();
                        System.out.println("Le monstre vous a donné un indice : <<Uth...>>");
                        System.out.println("Il vaut mieux conserver cela dans un coin, il pourrait nous servir.");
                    }
                }
            }else if(currentRoom == rf.getRoom(2, 2)){
                if(!currentRoom.getVisited()){
                    //Génération d'un questions-réponses avec cette méthode
                    gameEnigma();
                }else{
                    System.out.println("Plus aucun champs de force émanent de cette pièce!");
                }
            }else if(currentRoom == rf.getRoom(4, 1)){
                System.out.println("Attention mon roi, il s'agit d'un piège!");
                System.out.println("Vous êtes directement redirigé vers l'exterieur de la grotte...");
                currentRoom = rf.getRoom(0, 1);
            }else if(currentRoom == rf.getRoom(3, 0)){   
                if(!currentRoom.getVisited()){
                    //Dernière room où se trouve le graal
                    System.out.println("Félicitation mon roi, vous avez réussi à passer avec succès tout les différentes épreuves qui se sont dressées contre vous!");
                    System.out.println("Vous voici maintenant dans la salle du Graal, néanmoins pour accèder il est demandé un code secret...");
                    System.out.println("Savez-vous du quel code il s'agit ?");
                    System.out.println("Rassemblez toutes les données récupérées durant notre périple, cela nous aidera forcement.");
                    System.out.print("Entrer un code secret: ");
                    Scanner sc = new Scanner(System.in);
                    String str = sc.nextLine();
                    while(!str.equals("Uther Pendragon")){
                        System.out.print("Essayer à nouveau : ");
                        str = sc.nextLine();
                    }
                   
                    // Le jeu est fini, le joueur à remporté la partie!!!
                    System.out.println("Félicitation mon roi vous avez donner le bon code!");
                    System.out.println("Le Graal est à vous!");
                    System.out.println("Les Dieux avaient raison, vous êtes l'élu!");
                    System.out.println("Fin... A la prochaine pour de nouvelle aventure!");
                    
                }else{
                    System.out.println("Vous avez déjà trouvé le code secret!");
                }
                System.out.println("Tapez sur 'quit' pour quitter le jeu.");
            }else{
                System.out.print("Voici les différentes sorties : ");
                if (currentRoom.getNorthExit() != null) {
                    System.out.print("Nord ");
                }
                if (currentRoom.getEastExit() != null) {
                    System.out.print("Est ");
                }
                if (currentRoom.getSouthExit() != null) {
                    System.out.print("Sud ");
                }
                if (currentRoom.getWestExit() != null) {
                    System.out.print("Ouest ");
                }
                System.out.println();
            }
            currentRoom.setVisited();
        }    
        
        gv.showStatus(rf, currentRoom, arthur, null);
    }

    /** 
     * Méthode permettant de fouiller le coin d'une pièce
     * @param command La commande à analyser.
     */
    private void searchRoom(Command command) 
    {
        if (!command.hasSecondWord()) {
            // Si il n'y a pas de second mot...
            System.out.println("Vous voulez chercher vers où?");
            return;
        }

        String direction = command.getSecondWord();

        if (direction.equals("nord")) {
            randomizeObject();      
        }
        
        if (direction.equals("est")) {
            randomizeObject();
        }
        
        if (direction.equals("sud")) {
            randomizeObject();
        }
        
        if (direction.equals("ouest")) {
            randomizeObject();
        }
    }
    
     /**
      * Méthode permettant d'executer l'action d'attaquer envers l'ennemi
      * @param monster   Il s'agit du monstre attaqué
      */ 
    private void attackEnnemy(){
        if(monster.isAlive()){
            int damagePlus = monster.getLife() - ( (arthur.getDamage() + ((Gear) arthur.getBag().get(0)).getAttPlus() )- monster.getDefence() );
            monster.setLife(damagePlus);
            System.out.println("Vous avez attaquer " + monster.getName() + " et lui avez fait perdre " +  
                                ( ((int)(arthur.getDamage()*arthur.getChance()) )- monster.getDefence() ) 
                                + " de points de vie!");    
        }
    }
 
    /**
     * Méthode permettant d'executer l'action de defendre envers l'ennemi
     */
    private void defend(){
        if(arthur.getDefence() < monster.getDamage()){
            arthur.setDefence(arthur.getDefence() + ((Gear) arthur.getBag().get(1)).getDefPlus() + ((Gear) arthur.getBag().get(2)).getDefPlus());
            System.out.println("Vous augmentez votre garde !");
        }else{
            System.out.println("Votre défence est déjà optimale !");
        }
    }
    
     /**
     * Méthode permettant d'executer l'action de fuite lors d'un combat
     */

    private boolean escape(){
        if(arthur.getChance() > (monster.getLife()/monster.getDefence())){  
            modeFight = false;
            System.out.println("Vous avez réussi à fuire le combat!");
        }else{
            System.out.println("Vous ne pouvez pas fuire le combat!");
        }
        return modeFight;
    }

     /**
         * Méthode permettant d'executer l'action de boire une potion
         * @param potion   Il s'agit de la potion à boire
         */
        private void drink(Command command){
            
            //Permet de savoir si l'on a trouvé un potion dans son sac
            boolean trouve = false;  
            
            //Récupération de la seconde commande
            String typePotion = command.getSecondWord();
            HashMap<Integer, Object> sac = arthur.getBag();
            //Vérification s'il existe une potion dans le sac
            for (int i = 0; i<10; i++){ //On vérifie tout les entrées du sac
                
                if(sac.get(i) instanceof Potion){
                    int plus = ((Potion) sac.get(i)).getPlus(); 
                    if(typePotion.equals( ((Potion) sac.get(i) ).getName() ) ){
                        
                        if( 350 >=  arthur.getLife() + plus){
                            arthur.setLife(arthur.getLife() + plus);
                        }else{
                            arthur.setLife(350);
                        }
                        System.out.println("Vous avez bu une potion !");
                        player.startPlaying("audio/Potion.mp3");
                        trouve = true;
                        break;
                    }else if(typePotion.equals( ((Potion) sac.get(i) ).getName() )){
                        
                        arthur.setChance(arthur.getChance() + plus);
                        System.out.println("Vous avez bu une potion !");
                        player.startPlaying("audio/Potion.mp3");
                        trouve = true;
                        break;
                    }
                }
            }
            if(!trouve){
                System.out.println("Vous n'avez pas de potion dans votre sac...");
            }        
        }
    
    /** 
     * Méthode permettant de quitter le jeu.
     * @param command La commande à analyser.
     * @return true, si la commande est bien "quit"
     */
    private boolean quit(Command command) 
    {
        if (command.hasSecondWord()) {
            System.out.println("J'ai peur de ne pas avoir compris, vous voulez quitter quoi ?");
            return false;
        }
        else {
            return true;  // signal qu'il veut quitter le jeu
        }
    }
    
    /**
     * Méthode simulant un combat entre le héros et son ennemi
     */
    private void combat(){
        //Stop la musique actuelle
        player.stop();
        player.startPlaying("audio/Battle.mp3"); //Musique spéciale combat
        System.out.println("Veuillez saisir une action :");
        
        while(modeFight){
            if( monster.isAlive() && arthur.isAlive() ){    //On vérifie si le monstre est toujours vivant
                Command command = parser.getCommand();
                String commandWord = command.getCommand();
                Random rand = new Random();
                if (commandWord.equals("att")){
                    
                    //Nombre random qui défini l'action que va entreprendre le monstre après votre propre action (4 choix possibles)
                     int i = rand.nextInt(3 - 1 + 1) + 1;
                     attackEnnemy();
                     
                     //Action aléatoire de l'ennemi
                     switch (i)
                     {
                       case 1:
                         
                         System.out.println(monster.getName() + " vous attaque !");
                         monster.attack(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                       case 2:
        
                         System.out.println(monster.getName() + " vous lance un sort!");
                         monster.fate(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                       case 3:
        
                         System.out.println(monster.getName() + " se défend!");
                         monster.defend(arthur);
                         affichageCombat();
                         break;
                     }
                 }
                 if(commandWord.equals("def")){
                     int i = rand.nextInt(2 - 1 + 1) + 1;
                     defend();
                     gv.showStatus(rf, currentRoom, arthur, null);
                     switch (i)
                     {
        
                       case 1:
        
                         System.out.println(monster.getName() + " vous lance un sort!");
                         monster.fate(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                       case 2:
        
                         System.out.println(monster.getName() + " vous vole un objet!");
                         monster.rob(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                     }
                 }
                 if(commandWord.equals("drink")){
                     int i = rand.nextInt(2 - 1 + 1) + 1;
                     drink(command);
                     gv.showStatus(rf, currentRoom, arthur, null);
                     switch (i)
                     {
        
                       case 1:
        
                         System.out.println(monster.getName() + " vous attaque !");
                         monster.attack(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                       case 2:
        
                         System.out.println(monster.getName() + " vous vole un objet!");
                         monster.rob(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                     }
                 }
                 if(commandWord.equals("esc")){
                     int i = rand.nextInt(2 - 1 + 1) + 1;
                     escape();
                     switch (i)
                     {
        
                       case 1:
        
                         System.out.println(monster.getName() + " vous attaque !");
                         monster.attack(arthur);
                         affichageCombat();
                         gv.showStatus(rf, currentRoom, arthur, null);
                         break;
        
                       case 2:
                         
                         System.out.println(monster.getName() + " se défend!");
                         monster.defend(arthur);
                         affichageCombat();
                         break; 
                     }  
                 }  
            }else{
                
                if(!monster.isAlive()){
                   System.out.println("Vous avez vaincu " + monster.getName() + " !");
                   System.out.println("Félicitation mon roi !");
               }else{
                   System.out.println("Vous avez été vaincu mon roi...");
                   System.out.println("Ne perdez pas espoir et essayer de nouveau, ");
                   System.out.println("soit depuis votre sauvegarde avec la commande 'load' ou depuis le début en executant à nouveau le jeu!");
                   finished = true;
                }
               modeFight = false;
            }
        }
        player.stop();
        player.startPlaying("audio/Medieval-Theme.mp3");
    }
    
      /**
     * Méthode permettant l'affichage sommaire des donnée de chaque adversaire
     * @param c ennemy à combattre
     */
    private void affichageCombat(){
        
        //Affichage des données des deux joueurs
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.print("|     " + arthur.getName());
        System.out.print("     |     " + "PV : " + arthur.getLife() + " / 350");
        System.out.print("     |     " + "Def : " + arthur.getDefence() + "     |");
        System.out.print("     " + "Chance : " + arthur.getChance() + "     |");
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------");
        
        System.out.println(); 
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.print("|     " + monster.getName());
        System.out.print("     |     " + "PV : " + monster.getLife());
        System.out.print("     |     " + "Def : " + monster.getDefence());
        System.out.print("     |     " + "Description : " + monster.getDescription());
        System.out.println("     |");
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------------");
            
    }
    
    /**
     * Méthode permettant de lancer un jeu d'énigme avec l'utilisateur.
     */
    private void gameEnigma()
    {
        player.stop();
        player.startPlaying("audio/enigme.mp3"); //musique spéciale enigme
        
        Enigme enigme = new Enigme();
        System.out.println("Bienvenue mon roi, je suis la dame du lac...");
        System.out.println("Avant de pouvoir continuer votre quête, les Dieux souhaitent tester votre intelligence...");
        System.out.println();
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Vous ne pourrez quitter cette pièce sans avoir répondu à ces trois enigmes, souhaitez-vous sauvegarder avant ? (Oui ou Non)");

        String str = sc.nextLine();
        if(str.equals("oui")){
            try{
                    saving();
                }catch (FileNotFoundException e){
                    //T0D0 Auto-generated catch block
                    e.printStackTrace();
                }catch (IOException e) {
                    //T0D0 Autogenerated catch block
                    e.printStackTrace();
            }
        }
        
        System.out.println("Première enigme : ");
        System.out.println();
        while(!enigme.start("La faim")){}
        System.out.println();
  
        System.out.println("Seconde enigme : ");
        while(!enigme.start("Hier Hervé a acheté des cuillières")){}
        System.out.println();
        
        System.out.println("Dernière enigme : ");
        while(!enigme.start("Mon père")){}
        
        player.stop();
        player.startPlaying("audio/Medieval-Theme.mp3");
    }

     /**
     * Methode permettant de choisir dans l'attribut obj une potion, un trap ou un monstre au hasard
     */
    private void randomizeObject(){
        //savoir si l'on a trouvé quelque chose
        boolean trouve = false;
        
        //initialisation de i
        int i = 0;
        Random rand = new Random();
        //On verifie si la room actuelle est un campement ; Si oui on change la valeur random de i afin de ne trouver que des monstres dans ce type de room
        if (currentRoom == rf.getRoom(1, 0)){
           i = rand.nextInt(102 - 97 + 1) + 97; //Interval où se trouve seulement des truands
        }else if(currentRoom == rf.getRoom(1, 2)){
            i = rand.nextInt(96 - 91 + 1) + 91; //Interval où se trouve seulement des gobelins
        }else if(currentRoom == rf.getRoom(2, 0)){
            i = 105; //Correspond à la place du boss capalus dans obj
        }else if(currentRoom == rf.getRoom(0, 3)){
            i = 106; //Correspond à la place du boss glatissante dans obj
        }else if(currentRoom == rf.getRoom(3, 2)){
            i = 107; //Correspond à la place du boss roi des gobelin dans obj
        }else if(currentRoom == rf.getRoom(3, 1)){
            i = 108; //Correspond à la place du boss dragon dans obj
        }else if(currentRoom == rf.getRoom(2, 2)){
            
        }else{
            //Choix d'un nombre au hazard (On prend comme valeur max 90 afin de réduire la chance de trouver n quelconque objet) 
            i = rand.nextInt(90 - 0 + 1) + 0;
        }
        
       
        //Vérification de l'objet trouver avec les différentes classes
        if(obj.get(i) instanceof Potion) {
            System.out.println("Vous avez trouvé : " + ((Potion)obj.get(i)).getName());
            
            //On vérifie s'il y a assez de place dans le sac d'arthur(la valeur 10 est donné arbitrairement)
            if(arthur.getBag().size() > 10){
                System.out.println("Malheureusement votre sac est plein vous ne pouvez pas emporter cette potion.");
            }else{
                arthur.addBag(obj.get(i));
            }
            gv.showStatus(rf, currentRoom, arthur, null);
           
        }else if(obj.get(i) instanceof Trap){       
                Trap trap =(Trap) obj.get(i);
                arthur.setLife(arthur.getLife() - (int)trap.getMinus());
                System.out.println("Vous avez etait en contact avec un " + ((Trap)obj.get(i)).getName() );
                
                //On vérifie s'il s'agit d'une potion de chance ou d'un Elixir afin d'adapter l'affichage
                if(((Trap)obj.get(i)).getName() == "PoisonChance"){
                    System.out.println("Vous avez perdu " + ((Trap)obj.get(i)).getMinus() + " de chance.");
                }else{
                    System.out.println("Vous avez perdu " + ((Trap)obj.get(i)).getMinus() + " de points de vie.");
                }
                gv.showStatus(rf, currentRoom, arthur, null);
                
        }else if(obj.get(i) instanceof Monster){
                if(((Monster)obj.get(i)).getLife() != 0){
                    modeFight = true;
                    monster =(Monster) obj.get(i);
                    printMerlin();
                    affichageCombat();
                    combat();

                }else{
                    randomizeObject();
                }
        }else{
            System.out.println("Vous n'avez rien trouvé dans ce coin-ci...");
        }

    }

    /**
     * Méthode permettant la sauvegarde de l'état du jeu
     * @throws FileNoteFoundException
     * @throws IOException
     */
    
    private void saving()throws FileNotFoundException, IOException{
       //Sauvegarde des données
       Save save = new Save();
       save.setArthur(arthur);
       save.setRoomsField(rf);
       save.setCurrentRoom(currentRoom);
       save.setObj(obj);
       
       //Sauvegarde des rooms de facon non optimisé
       save.setExtGrotte(rf.getRoom(0, 1));
       save.setIntGrotte(rf.getRoom(1, 1));
       save.setCmpTruand(rf.getRoom(1, 0));
       save.setBossCapalus(rf.getRoom(2,0));
       save.setCmpGobe(rf.getRoom(1, 2));
       save.setCouloiSerpent(rf.getRoom(0, 2));
       save.setBossGlatissante(rf.getRoom(0, 3));
       save.setSalleLancelot(rf.getRoom(1, 3));
       save.setSalleEnigme(rf.getRoom(2, 2));
       save.setBossGobe(rf.getRoom(3, 2));
       save.setSalleMarchand(rf.getRoom(4, 2));
       save.setTresorGobe(rf.getRoom(3, 3));
       save.setCorridorD(rf.getRoom(2, 1));
       save.setBossDragon(rf.getRoom(3, 1));
       save.setSalleTele(rf.getRoom(4, 1));
       save.setGrall(rf.getRoom(3, 0));
       
       //Mise en place de la sauvegarde dans flux d'objet sortant et dans flux de fichier sortant
       FileOutputStream fos = new FileOutputStream("Sauvegarde.txt");
       ObjectOutputStream oos = new ObjectOutputStream(fos);
       oos.writeObject(save);
       oos.close();
       System.out.println("Vous avez bien sauvegarder votre partie.");
    }
    
    /**
     * Méthode permettant la lecture du fichier de sauvegarde
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    
    private void loader() throws FileNotFoundException, IOException, ClassNotFoundException{
        FileInputStream fis = new FileInputStream("Sauvegarde.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Save save = (Save) ois.readObject();
        ois.close();
        arthur.transformIn(save.getArthur());
        currentRoom.transformIn(save.getCurrentRoom());
        rf.transformIn(save.getRoomsField());
        
        //Sauvegarde du HashMap où se situe les monstre, potion et trap
        for(int i = 0; i<=save.getObj().size() ; i++){
                if(save.getObj().get(i) instanceof Potion){
                    Potion potion = (Potion) obj.get(i);
                    potion.transformIn((Potion)save.getObj().get(i));
                }
                
                if(save.getObj().get(i) instanceof Trap){
                    Trap trap = (Trap) obj.get(i);
                    trap.transformIn((Trap)save.getObj().get(i));
                }
                
                if(save.getObj().get(i) instanceof Monster){
                    Monster monster = (Monster) obj.get(i);
                    monster.transformIn((Monster) save.getObj().get(i));
                }
        }
       // obj.tranformIn(save.getGrall());
        
        // Chargement des rooms non optimisé
        rf.getRoom(0, 1).transformIn(save.getExtGrotte());
        rf.getRoom(1, 1).transformIn(save.getIntGrotte());
        rf.getRoom(1, 0).transformIn(save.getCmpTruand());
        rf.getRoom(2, 0).transformIn(save.getBossCapalus());
        rf.getRoom(1, 2).transformIn(save.getCmpGobe());
        rf.getRoom(0, 2).transformIn(save.getCouloiSerpent());
        rf.getRoom(0, 3).transformIn(save.getBossGlatissante());
        rf.getRoom(1, 3).transformIn(save.getSalleLancelot());
        rf.getRoom(2, 2).transformIn(save.getSalleEnigme());
        rf.getRoom(3, 2).transformIn(save.getBossGobe());
        rf.getRoom(4, 2).transformIn(save.getSalleMarchand());
        rf.getRoom(3, 3).transformIn(save.getTresorGobe());
        rf.getRoom(2, 1).transformIn(save.getCorridorD());
        rf.getRoom(3, 1).transformIn(save.getBossDragon());
        rf.getRoom(4, 1).transformIn(save.getSalleTele());
        rf.getRoom(3, 0).transformIn(save.getGrall());
        
       
        printSituation();
    }
}
