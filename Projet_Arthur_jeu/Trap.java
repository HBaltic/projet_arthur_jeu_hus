
/**
 * Description de la classe Trap qui hérite de la classe Item
 * 
 * @author (tetsatls) 
 * @version (v1)
 */
public class Trap extends Item
{
    //attributs de la classe 
    private String name;
    private int minus;
    
    /**
     * constructeur de classe qui prend 2 paramètres
     * @param name le nom de la Trap
     * @param minus la valeur a retirer (valeur pénalisante)
     */
    public Trap(String name, int minus) {
        super(name);
        this.minus = minus;
    }
    
    /**
     * Getter qui retourne minus
     * @return L'attribut minus
     */
    public int getMinus() {
        return this.minus;
    }
       
    /**
     * Transforme les données de sauvegarde en objet Trap
     *
     * @param data données à transformer
     */
    public void transformIn(Trap data)
    {
        this.name = data.getName();
        this.minus = data.getMinus();
    }
}

