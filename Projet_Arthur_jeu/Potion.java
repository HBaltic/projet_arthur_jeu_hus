
/**
 * Description de la classe Potion qui hérite de la classe Item
 * 
 * @author (tetsatls) 
 * @version (v1)
 */
public class Potion extends Item
{
    //attributs de la potion
    private String name;
    private int plus;
    
    /**
     * constructeur de classe qui prend 2 paramètres
     * @param name  le nom de la potion
     * @param plus la quantité de potion gagnée
     */
    public Potion(String name, int plus) {
        super(name);
        this.plus = plus;
    }
    
    /**
     * Getter qui retourne la quantité 'plus"
     * @return plus
     */
    public int getPlus() {
        return this.plus;
    }
       
     /**
     * Transforme les données de sauvegarde en objet Potion
     *
     * @param  data        données à transformer
     */
    public void transformIn(Potion data)
    {
        this.name = data.getName();
        this.plus = data.getPlus();
    }
}
