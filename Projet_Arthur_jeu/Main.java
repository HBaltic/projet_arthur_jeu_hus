
/**
 * Classe main permet de lancer le jeu
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Main
{
    /**
     * Constructeur d'objets de classe Main
     */
    public Main()
    {
        Game game = new Game();
        game.play();
    }

}
