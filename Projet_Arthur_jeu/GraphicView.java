import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import javax.imageio.*;

/**
 * A graphical view of the game.
 * The view displays a colored map for each location 
 * representing its rooms. It uses a default background color.
 * Colors for each type of species can be defined using the
 * setColor method.
 * 
 * @author habsaoui2u
 * @version 2015.03.21
 */
public class GraphicView extends JFrame implements ActionListener
{
    // Color for room with no character.
    private static final Color ROOM_NOVISIT_COLOR = Color.GRAY;
    // Color for room with no character.
    private static final Color ROOM_VISIT_COLOR = Color.WHITE;
    // Color for empty with a character.
    private static final Color ROOM_CHARACTER_COLOR = Color.ORANGE;
    // Color for bridges between rooms.
    private static final Color ROOM_BRIDGE_COLOR = Color.GRAY;
    // Color for zoom room.
    private static final Color ROOM_ZOOM_COLOR = Color.BLUE;    
    
    // The Pane to draw the field on it
    private FieldView fieldView;
    
    // The field representing all the rooms
    private RoomsField field;
    // The current room the hero stills in
    private Room heroRoom;
    // The Hero character involving on field
    private Hero hero;
    // The current field position of the hero
    private int[] heroPos;
    // The direction taken by the hero
    private String direction;
    // The background image to draw on Frame's panel
    Image fieldImg;
    // The stats icons to draw
    Image heroImg, lifeImg, defenceImg, damageImg, gearImg;
    //The scrollbars for stats' level
    JProgressBar lifeLevel, defenceLevel, damageLevel;
    //The text field to enter commands
    JTextField textField;
    // The command line written in text field
    String cdeLine; 
    //The text area to display commands
    JTextArea textArea, textArea2;
        
    /**
     * Create a view of the given width and height.
     * 
     */
    public GraphicView()
    {
        // Set frame's properties
        setTitle("ARTHUR THE GAME"); 
        setLocation(10, 10);
        setPreferredSize(new Dimension(1100, 1000));
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon("images/Arthur.jpg").getImage());        
         // Init the field view of the game 
        fieldView = new FieldView(1100, 900);
        
        // Text area to display comments
        textArea = new JTextArea("Arthur relatera ici sa quête du Graal ! "); 
        // Set Font of textField
        Font textFont = new Font("Arial ",Font.BOLD, 15);
        textArea.setFont(textFont); 
        // position the JTextField
        textArea.setBounds(50, 650, 520, 100);
        // Set automatic line return
        textArea.setWrapStyleWord(true);
        textArea.setLineWrap(true);
        
        // Text area to display room description
        textArea2 = new JTextArea(); 
        // Set Font of textField
        textFont = new Font("Arial ",Font.BOLD, 15);
        textArea2.setFont(textFont); 
        // position the JTextField
        textArea2.setBounds(695, 395, 300, 200);
        // Set automatic line return
        textArea2.setWrapStyleWord(true);
        textArea2.setLineWrap(true);
        
        //The textfield to type commands
        textField = new JTextField("Saisir commande..."); 
        // Set Font of textField
        textField.setFont(textFont); 
        // position the JTextField
        textField.setBounds(50, 800, 250, 30);
        // Listener for mouse and keyboard actions
        textField.addActionListener(this);
        textField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                textFieldMousePressed(evt);
            }
        });
        
        //The hero stats levels
        // hero lifelevel bar
        lifeLevel = new JProgressBar();    
        lifeLevel.setMaximum(350); 
        lifeLevel.setMinimum(0); 
        lifeLevel.setStringPainted(true);
        lifeLevel.setForeground(Color.MAGENTA);
        // position the progressbar
        lifeLevel.setBounds(740, 690, 200, 20);
        
        // hero defenceLevel bar
        defenceLevel = new JProgressBar(); 
        defenceLevel.setMaximum(100); 
        defenceLevel.setMinimum(0);
        defenceLevel.setStringPainted(true);
        defenceLevel.setForeground(Color.GREEN);
        // position the progressbar
        defenceLevel.setBounds(740, 730, 200, 20);
        // hero damageLevel bar
        damageLevel = new JProgressBar();  
        damageLevel.setMaximum(100); 
        damageLevel.setMinimum(0);  
        damageLevel.setStringPainted(true);
        damageLevel.setForeground(Color.GRAY);
        // position the progressbar
        damageLevel.setBounds(740, 770, 200, 20);       
       
        // Init useful variables
        // The field representing all the rooms
        field = new RoomsField(0, 0);
        // The current room the hero stills in
        heroRoom = new Room("");
        // The Hero character involving on field
        hero = new Hero("", "", 0, 0, 0, 0.0);
        // The current field position of the hero
        heroPos = new int[2];
        // The direction taken by the hero
        direction = "";  
               
        // Load background image for the field and icon images for stats         
        try {
           this.fieldImg = ImageIO.read(new File("images/Field.jpg"));
           this.heroImg = ImageIO.read(new File("images/Hero.png"));
           this.lifeImg = ImageIO.read(new File("images/Life.png"));
           this.defenceImg = ImageIO.read(new File("images/Defence.png"));
           this.damageImg = ImageIO.read(new File("images/Damage.png"));
           this.gearImg = ImageIO.read(new File("images/Gear.png"));
        } 
        catch (IOException e) {
           e.printStackTrace();
        }
        
        // Adding contents to the frame
        getContentPane().add(textField);
        getContentPane().add(textArea);
        getContentPane().add(textArea2);
        getContentPane().add(lifeLevel);
        getContentPane().add(defenceLevel);
        getContentPane().add(damageLevel);
        getContentPane().add(fieldView);
        
        // Put Frame and contents together
        pack();
        setVisible(true);  
    }
    
    /**
     * Show the current status of the field.
     * @param step Which iteration step it is.
     * @param field The field whose status is to be displayed.
     */
    public void showStatus(RoomsField field, Room heroRoom, Hero hero, String direction)
    {
        // upadate useful variables
        this.field = field;         
        this.heroRoom = heroRoom;
        this.hero = hero;
        this.direction=direction;
        
        // Update drawing
        fieldView.repaint();
        lifeLevel.repaint();
        defenceLevel.repaint();
        damageLevel.repaint();
        textField.repaint();
        textArea.repaint();
        textArea2.repaint();
    }
    
    /**
     * Clear text field on mouse click.
     */
    private void textFieldMousePressed(java.awt.event.MouseEvent evt) {
          textField.setText("");
    }
    
    /**
     * Show text when user presses ENTER.
     */ 
    public void actionPerformed(ActionEvent ae) { 
      // read text field
      this.cdeLine = this.textField.getText();      
      // clear text field
      this.textField.setText("");
      display(this.cdeLine);
    }
      
    
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == e.VK_LEFT) {
            heroRoom = heroRoom.getEastExit();
            heroRoom.setVisited();
        } 
        else if (e.getKeyCode() == e.VK_RIGHT) {
            heroRoom = heroRoom.getWestExit();
            heroRoom.setVisited();
        } 
        else if (e.getKeyCode() == e.VK_UP) {
            heroRoom = heroRoom.getNorthExit();
            heroRoom.setVisited();
        }
        else if (e.getKeyCode() == e.VK_DOWN) {
            heroRoom = heroRoom.getSouthExit();
            heroRoom.setVisited();
        }
    }
    
    /**
     * Return the entered commands in textField.
       * 
     * @return  The string line written in textField.
     */
    public String getCommand()
    {
        return this.cdeLine;
    }
    
    /**
     * Display commentaries about the game.
     */
    public void display(String comments)
    {
        // Update display of commentaries
        this.textArea.insert("> "+comments+"\n", 0);
    }
    

    /**
     * Provide a graphical view of a rectangular field. This is 
     * a nested class (a class defined inside a class) which
     * defines a custom component for the user interface. This
     * component displays the field.
     * This is rather advanced GUI stuff - you can ignore this 
     * for your project if you like.
     */
    private class FieldView extends JPanel 
    {   
        // rooms field size
        private int fieldWidth;
        private int fieldHeight;
        
        /**
         * Create a new FieldView component.
         */
        public FieldView(int width, int height)
        {
           // Init field and room size
           fieldWidth = width;
           fieldHeight = height;           
           // Init Pane's size and color
           setPreferredSize(new Dimension(width, height));
           // Set an action listener for textField
        }
           
        /**
         * The field view component needs to be redisplayed. 
         * Copy the internal image to screen.
         * 
         * @param g Graphics to redisplay
         */
        public void paintComponent(Graphics g)
        {
           // Draw a background image on the field           
           g.drawImage(fieldImg, 0, 0, this.getWidth(), this.getHeight(), this);
            
           // Draw the discovered field rooms
           for (int y = 0; y < field.getHeight(); y++) {
              for (int x = 0; x < field.getWidth(); x++) {
                   // if the room exists and is visited by the Hero
                   if( field.getRoom(x, y)!=null && field.getRoom(x, y).getVisited()){                    
                       // Check and save the current position of the Hero in this room
                       if( field.getRoom(x, y) == heroRoom ){ 
                           heroPos[0] = x; 
                           heroPos[1] = y;
                       }                        
                       
                       // Draw room visited by the hero and its possible directions
                       if( field.getRoom(x, y).getNorthExit() != null )
                           fieldView.drawRoom(x, y, "north", g);
                       if( field.getRoom(x, y).getSouthExit() != null )
                           fieldView.drawRoom(x, y, "south", g);
                       if( field.getRoom(x, y).getEastExit() != null )
                           fieldView.drawRoom(x, y, "east", g);
                       if( field.getRoom(x, y).getWestExit() != null )
                           fieldView.drawRoom(x, y, "west", g);
                  }
               }
           }   
           
           drawZoom(heroRoom, hero, g);
        }  
    
        /**
         * Draw on field location all visited rooms in a given color.
         */
        public void drawRoom(int x, int y, String direction, Graphics g)
        {
           // Draw position of the Hero character on field                
           if(heroPos[0]==x && heroPos[1]==y)
            g.setColor(ROOM_CHARACTER_COLOR); 
           else if(field.getRoom(x, y).getVisited())
            g.setColor(ROOM_VISIT_COLOR); 
           else
            g.setColor(ROOM_NOVISIT_COLOR);
                       
           // Draw a Polygon representing a room
           int z = 80;
           int x2[] = {    (x*z)+50, (x*z)+60, (x*z)+80, (x*z)+90, 
                           (x*z)+90, (x*z)+80, (x*z)+60, (x*z)+50      };   
                           
           int y2[] = {    (y*z)+60, (y*z)+50, (y*z)+50, (y*z)+60, 
                           (y*z)+80, (y*z)+90, (y*z)+90, (y*z)+80      };  
                            
           g.fillPolygon(x2, y2, 8);           
           
           // Draw the Bridge(s) between the current room and it's close visited ones
           g.setColor(ROOM_BRIDGE_COLOR); 
           if( direction == "north")
               // Draw north bridge
               g.fillRect( x2[1], y2[1]-(y2[6]-y2[1]), x2[2]-x2[1], y2[6]-y2[1]);
           else if( direction == "south")
               // Draw south bridge
               g.fillRect( x2[6], y2[6], x2[5]-x2[6], y2[6]-y2[1]);
           else if( direction == "west")
               // Draw east bridge
               g.fillRect( x2[0]-(x2[3]-x2[0]), y2[0], x2[3]-x2[0], y2[7]-y2[0]);
           else if( direction == "east")
               // Draw west bridge
               g.fillRect( x2[3], y2[3], x2[3]-x2[0], y2[4]-y2[3]);
               
           g.setColor(Color.RED);
           g.drawString(x+"/"+y, x2[1], y2[1]);
        } 
        
        /**
         * Draw a zoomed view of the Hero's current location room.
         */
        public void drawZoom(Room heroRoom, Hero hero, Graphics g)
        {                        
            // Draw the separator
            Graphics2D g2d = (Graphics2D)g;
            GradientPaint grad = new GradientPaint(0, 0, Color.WHITE, 0, getHeight(), Color.BLACK);
            g2d.setPaint(grad);
            int x=650, y=25;
            g2d.fillRoundRect(x, y, 400, 850, 20, 20);
           
            // Draw the zoomed room on the right of field
            grad = new GradientPaint(0, 0, Color.BLUE, 0, getHeight(), Color.YELLOW);
            g2d.setPaint(grad);           
            int l1=70, l2=120;
            int[] x2 = {    x+l1, x+l1+l1, x+l1+l1+l2, x+l1+l1+l2+l1, x+l1+l1+l2+l1, 
                            x+l1+l1+l2+l1-l1, x+l1+l1+l2+l1-l1-l2, x+l1+l1+l2+l1-l1-l2-l1};  
                            
            int[] y2 = {    y+l2, y+l2-l1, y+l2-l1, y+l2-l1+l1,  y+l2-l1+l1+l2, 
                            y+l2-l1+l1+l2+l1, y+l2-l1+l1+l2+l1, y+l2-l1+l1+l2+l1-l1};  
            g2d.fillPolygon(x2, y2, 8);
            
            // Draw the descpription of this room
            grad = new GradientPaint(0, 0, Color.BLUE, 0, getHeight(), Color.YELLOW);
            g2d.setPaint(grad);
            g2d.fillRoundRect( x2[7]-50, y2[7]+100, 350, 250, 20, 20);
            Font font = new Font("Arial ",Font.BOLD, 20);
            g.setFont(font);
            g.setColor(Color.BLACK);
            textArea2.setText("");
            textArea2.insert("> "+heroRoom.getDescription()+"\n", 0);
            
            // Write the game stats' info and items bag of the hero
            grad = new GradientPaint(0, 0, Color.BLUE, 0, getHeight(), Color.YELLOW);
            g2d.setPaint(grad);
            g2d.fillRoundRect(x2[7]-50, y2[7]+370, 350, 220, 20, 20);
            font = new Font("Arial ",Font.BOLD, 15);
            g.setFont(font);
            g.setColor(Color.BLACK);
            
            // Draw the stats icons images            
            g.drawImage(heroImg, x2[7]-30, y2[7]+380, 30, 30, this);
            g.drawImage(lifeImg, x2[7]-30, y2[7]+420, 30, 30, this); 
            g.drawImage(defenceImg, x2[7]-30, y2[7]+460, 30, 30, this); 
            g.drawImage(damageImg, x2[7]-30, y2[7]+500, 30, 30, this);  
            g.drawImage(gearImg, x2[7]-30, y2[7]+540, 30, 30, this); 
            
            //  Draw the stats progressbars + other things
            // Display hero name
            g.drawString(((Character)(hero)).getName().toUpperCase(), x2[7]+20, y2[7]+405);
            // Display the hero's life level
            lifeLevel.setValue( ((Character)(hero)).getLife() ); 
            //lifeLevel.setForeground(Color.RED);
            lifeLevel.setString(""+((Character)(hero)).getLife());            
            // Display the hero's defence level
            defenceLevel.setValue( ((Character)(hero)).getDefence() );
            defenceLevel.setString(""+((Character)(hero)).getDefence());
            // Display the hero's damage level
            damageLevel.setValue( ((Character)(hero)).getDamage() );
            damageLevel.setString(""+((Character)(hero)).getDamage());
            // Display list of items the hero has in his bag
            g.drawString("GEAR: " + hero.afficherBag(), x2[7]+20, y2[7]+565);
            
            // Display comments to the text area
            g.drawString( "Arthur raconte...", 50, 640);
            // Display comments to the text area
            g.drawString( "Arthur décide...", 50, 790);
        }
        

    }
}
