import java.util.*;
/**
 * Classe qui pose une question au héro 
 * et récupère la réponse de ce dernier
 * @author tetsatls
 * @version V1
 */
public class Enigme
{
    /**
     * attribut Responder
     */
    
    public Responder responder;

    /**
     * Créer un constructeur par défaut
     */
    public Enigme()
    {
        responder = new Responder();
    }
    
    /**
     *  Génère le dialogue entre le héro et un donneur d'énigme
     *  @param  key la clé de la question correspondante
     *  dans la collection de questions
     *  @return trouve si le héro a trouvé la bonne réponse
     */
    public boolean start(String key)
    {
        boolean trouve = false;
        responder.fillResponseMap();
        responder.fillQuestionMap();
        responder.fillDefaultResponses();    
       
        System.out.println(responder.questionMap.get(key));
        Scanner sc = new Scanner(System.in);
        String reponse = sc.nextLine();
        if (reponse.equalsIgnoreCase(responder.responseMap.get(key))) {
            System.out.println("Bravo Sir Arthur!");
            trouve = true;
        }
        else {
            System.out.println(responder.pickDefaultResponse());
        }
        return trouve;
    }
}
