

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class MusicPlayerTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class MusicPlayerTest
{
    /**
     * Default constructor for test class MusicPlayerTest
     */
    public MusicPlayerTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void testStartSample()
    {
        MusicPlayer musicPla1 = new MusicPlayer();
        musicPla1.playSample("audio/potion.mp3");
    }

    @Test
    public void testSartPlaying()
    {
        MusicPlayer musicPla1 = new MusicPlayer();
        musicPla1.startPlaying("audio/potion.mp3");
    }

    @Test
    public void testStopPlay()
    {
        MusicPlayer musicPla1 = new MusicPlayer();
        musicPla1.startPlaying("audio/potion.mp3");
        musicPla1.stop();
        musicPla1.stop();
        musicPla1.stop();
    }
}



